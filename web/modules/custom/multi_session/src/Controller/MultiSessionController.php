<?php

namespace Drupal\multi_session\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;
use Drupal\multi_session\Entity\MultiSessionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MultiSessionController.
 *
 *  Returns responses for Multi session routes.
 */
class MultiSessionController extends ControllerBase implements ContainerInjectionInterface {


  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Constructs a new MultiSessionController.
   *
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   */
  public function __construct(DateFormatter $date_formatter, Renderer $renderer) {
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * Displays a Multi session revision.
   *
   * @param int $multi_session_revision
   *   The Multi session revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($multi_session_revision) {
    $multi_session = $this->entityTypeManager()->getStorage('multi_session')
      ->loadRevision($multi_session_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('multi_session');

    return $view_builder->view($multi_session);
  }

  /**
   * Page title callback for a Multi session revision.
   *
   * @param int $multi_session_revision
   *   The Multi session revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($multi_session_revision) {
    $multi_session = $this->entityTypeManager()->getStorage('multi_session')
      ->loadRevision($multi_session_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $multi_session->label(),
      '%date' => $this->dateFormatter->format($multi_session->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Multi session.
   *
   * @param \Drupal\multi_session\Entity\MultiSessionInterface $multi_session
   *   A Multi session object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(MultiSessionInterface $multi_session) {
    $account = $this->currentUser();
    $multi_session_storage = $this->entityTypeManager()->getStorage('multi_session');

    $langcode = $multi_session->language()->getId();
    $langname = $multi_session->language()->getName();
    $languages = $multi_session->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $multi_session->label()]) : $this->t('Revisions for %title', ['%title' => $multi_session->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all multi session revisions") || $account->hasPermission('administer multi session entities')));
    $delete_permission = (($account->hasPermission("delete all multi session revisions") || $account->hasPermission('administer multi session entities')));

    $rows = [];

    $vids = $multi_session_storage->revisionIds($multi_session);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\multi_session\MultiSessionInterface $revision */
      $revision = $multi_session_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $multi_session->getRevisionId()) {
          $link = $this->l($date, new Url('entity.multi_session.revision', [
            'multi_session' => $multi_session->id(),
            'multi_session_revision' => $vid,
          ]));
        }
        else {
          $link = $multi_session->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.multi_session.translation_revert', [
                'multi_session' => $multi_session->id(),
                'multi_session_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.multi_session.revision_revert', [
                'multi_session' => $multi_session->id(),
                'multi_session_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.multi_session.revision_delete', [
                'multi_session' => $multi_session->id(),
                'multi_session_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['multi_session_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
