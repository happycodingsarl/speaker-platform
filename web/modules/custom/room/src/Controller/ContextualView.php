<?php
/**
 * Created by Happy Coder
 */

namespace Drupal\room\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\speakers_main\GroupService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\Renderer;
use Drupal\group\Entity\GroupContent;

class ContextualView extends ControllerBase {

  /**
   * @var Renderer
   */
  protected $renderer;

  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Get the render array view of room list
   *
   * @return array|null
   */
  public function roomList() {

    // Get group id
    $groupId = \Drupal::service('speakers.group')->getEventGroupId();

    // get renderable array for view
    $view = views_embed_view('room', 'page_by_group', $groupId);

    // \Drupal::getContainer()->setParameter();
    return $view;
  }


}

