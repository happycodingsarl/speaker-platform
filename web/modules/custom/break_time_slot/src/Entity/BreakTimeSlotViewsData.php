<?php

namespace Drupal\break_time_slot\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Break time slot entities.
 */
class BreakTimeSlotViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
