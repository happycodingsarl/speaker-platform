<?php

namespace Drupal\multi_session;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for multi_session.
 */
class MultiSessionTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
