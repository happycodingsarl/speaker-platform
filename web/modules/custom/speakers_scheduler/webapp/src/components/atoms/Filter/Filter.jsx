import React from 'react'
import Select from 'react-select'

// Import standard style from this component
import './style.scss'

export default class Filter extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidUpdate(prevProps) {
    //console.log(this.props.events, 'unscheduled component');
    if (prevProps.selectedOption !== this.props.selectedOption) {

      // Update events & filters
      this.setState({
        'selectedOption': this.props.selectedOption
      })
      //console.log(this.state, 'state filter component')
    }
  }

  state = {
    selectedOption: null,
    isClearable: true
  }

  handleChange = selectedOption => {
    this.setState({selectedOption});
    this.props.onSelect(selectedOption);
    console.log(`Option selected:`, selectedOption);
  }

  toggleClearable = () =>
    this.setState(state => ({ isClearable: !state.isClearable }));

  render() {
    const {selectedOption, isClearable} = this.state;
    const {name, options} = this.props;

    return (
      <Select
        className={"list-filter"}
        value={selectedOption}
        onChange={this.handleChange}
        options={options}
        placeholder={name}
        isClearable={isClearable}
      />
    );
  }

}
