<?php

namespace Drupal\speakers_main\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Password\PasswordInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Form for the invitation.
 */
class Invitation extends FormBase implements ContainerInjectionInterface{

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The mail manager
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * User, that open this page.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $user;

  /**
   * The password service class.
   *
   * @var \Drupal\Core\Password\PasswordInterface
   */
  protected $password;

  /**
   * Active request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  public $request;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;
  /**
   * Creates a Invitation instance.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The mail manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Password\PasswordInterface $password
   *   The password service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(Request $request, EntityTypeManagerInterface $entity_type_manager, MailManagerInterface $mail_manager, LanguageManagerInterface $language_manager, PasswordInterface $password, MessengerInterface $messenger) {
    $this->request = $request;
    $this->entityTypeManager = $entity_type_manager;
    $this->mailManager = $mail_manager;
    $this->languageManager = $language_manager;
    $this->password = $password;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.mail'),
      $container->get('language_manager'),
      $container->get('password'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'invitation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $user = NULL, $externalEventId = NULL, $token = NULL) {
    $this->notice('New user on invitation page (user:' . (int) $user . '|externalEventId:' . (int) $externalEventId . '|externalEventId:' . (int) $token . ')');
    $errorMessage = "The requested page is not available. Please contact: Isabelle Bourzeix (ibourzeix@fdiworlddental.org)";

    // Checking the token
    $internalToken = \Drupal::config('speakers_civicrm.settings')->get('speakers_invitation_token', '');
    if ($internalToken != $token) {
      $this->error('Wrong token');

      return [
        'error' => [
          '#type' => 'hidden',
          '#value' => $errorMessage
        ],
      ];
    }

    $uid = $this->entityTypeManager
      ->getStorage('user')
      ->getQuery()
      ->condition('field_external_id', $user)
      ->execute();

    if (empty($uid)) {
      $this->error('Wrong user');

      return [
        'error' => [
          '#type' => 'hidden',
          '#value' => $errorMessage
        ],
      ];
    }

    // Check event
    $eventIds = \Drupal::entityQuery('event')->condition('field_civicrm_event_id', (string) $externalEventId)->execute();
    if (empty(reset($eventIds))) {
      $this->error('Wrong event');

      return [
        'error' => [
          '#type' => 'hidden',
          '#value' => $errorMessage
        ],
      ];
    }
    $eventId = reset($eventIds);

    // Set event id in session var
    $session = $this->request->getSession();
    $groupId = speakers_main_get_group_id($eventId);
    $session->set('selectedEvent', $eventId);
    $session->set('selectedGroupId', $groupId);

    $this->user = $this->entityTypeManager->getStorage('user')->load(reset($uid));

    // check if user was already accept invitation, and if yes - redirect him.
    $participant = $this->entityTypeManager
      ->getStorage('group_content')
      ->loadByProperties(['entity_id' => $uid]);
    if (!empty($participant) && reset($participant)->hasField('field_member_status') == TRUE &&
      reset($participant)->get('field_member_status')->getString() == 'onboarded'
    ) {
      $this->notice('User already on-boarded (user:' . (int) $user . ')');

      user_login_finalize($this->user);
      return new RedirectResponse(Url::fromRoute('entity.user.canonical', ['user' => $this->user->id()])->toString());
    }
    else {

      // Display invitation page with decline or accept action buttons
      $this->notice('Building invitation form (user:' . (int) $user . ')');

      // Get event
      $event_storage = $this->entityTypeManager
        ->getStorage('event');

      /* @var \Drupal\Event\Entity\Event $event */
      $event = $event_storage->load($eventId);
      if (is_null($event)) {
        $this->notice('event not found (user:' . (int) $user . '|externalEventId:' . (int) $externalEventId . ')');
        return [
          'error' => [
            '#type' => 'hidden',
            '#value' => $errorMessage
          ],
        ];
      }

      // Get event banner
      $file = NULL;
      if (!is_null($imageId = $event->get('field_banner_image')->target_id)) {
        $file = File::load($imageId);
      }

      if (!is_null($file)) {
        /* @var \Drupal\Core\Image\Image $image */
        $image = \Drupal::service('image.factory')->get($file->getFileUri());
        if ($image->isValid()) {
          $width = $image->getWidth();
          $height = $image->getHeight();
        }
        else {
          $width = $height = NULL;
        }
        if ($image->isValid()) {
          $form['banner'] = [
            '#theme' => 'image_style',
            '#width' => $width,
            '#height' => $height,
            '#style_name' => 'large',
            '#uri' => $file->getFileUri()
          ];
        }
      }

      $form['title'] = [
        '#type' => 'markup',
        '#markup' => $event->name->value . '<br>' .
          $event->get('field_city')->value . ' ' . $event->get('field_country')->value,
      ];
      $form['welcome'] = [
        '#type' => 'markup',
        '#markup' => $this->user->get('full_name_computed')->getValue(),
      ];
      $form['#tree'] = TRUE;

      $form['decline_block'] = [
        '#type' => 'fieldset',
      ];
      $form['decline_block']['message'] = [
        '#type' => 'markup',
        '#markup' => '<div class="decline-message"><p>Thank you for your reply.</p><p>We regret that you will not be able to contribute to the FDI WDC. <span style="font-weight:bold;">To confirm that you cannot attend, please click on Confirm.</p></div>',
      ];
      $form['decline_block']['decline_message'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Should you wish to leave a message, please write it below:'),
        //'#placeholder' => $this->t('Add a comment'),
        '#maxlength' => 500,
      ];
      $form['decline_block']['decline_submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('ConfirmDecline'),
        '#submit' => ['::submitDecline'],
        '#attributes' => [
          'class' => ['btn-lg'],
          'input_text' => $this->t('Confirm'),
        ],
      ];

      $form['accept_block'] = [
        '#type' => 'fieldset',
      ];
      $form['accept_block']['name'] = [
        '#type' => 'markup',
        '#markup' => '<div class="user-name">Dear ' . $this->user->get('full_name_computed')->getValue() . '</div>',
      ];
      $form['accept_block']['message'] = [
        '#type' => 'markup',
        '#markup' => '<div class="accept-message"><p>Thank you for accepting our invitation. We are delighted to have you as a speaker for the FDI WDC.</p><p style="font-weight:bold;">To confirm your attendance, please create a password and then click on Confirm below.</p><p>If you have any questions, do not hesitate to contact us.</p><p>&nbsp;</p><p style="font-style:italic;">Isabelle Bourzeix, Congress and Education Manager<br><a href="mailto:ibourzeix@fdiworlddental.org">ibourzeix@fdiworlddental.org</a></p><p>&nbsp;</p></div>',
      ];
      $form['accept_block']['email'] = [
        '#type' => 'markup',
        '#markup' => '<div class="user-email">Your login: ' . $this->user->getEmail() . '</div>',
      ];
      $form['accept_block']['pass_confirm'] = [
        '#type' => 'password_confirm',
        '#description' => t('Enter the same password in both fields'),
      ];
      $form['accept_block']['accept_submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('ConfirmAccept'),
        '#attributes' => [
          'class' => ['btn-lg'],
          'input_text' => $this->t('Confirm'),
        ],
        '#submit' => ['::submitAccept'],
      ];
    }

    // Display Suggested sessions @TODO filter by group
    $sessions = $this->entityTypeManager->getStorage('node')
      ->loadByProperties(['field_participant' => $uid, 'field_session_status' => 'suggested']);
    $this->notice(count($sessions) . ' suggested sessions (user:' . (int) $user . ')');

    $suggestedSessions = '';
    foreach ($sessions as $session) {
      $suggestedSession = '';
      $categories = $session->get('field_session_category')
        ->referencedEntities();
      if ($category = reset($categories)) {
        $suggestedSession .= $category->getName();
      }

      $topics = $session->get('field_session_topic')->referencedEntities();
      if ($topic = reset($topics)) {
        $suggestedSession .= ' > ' . $topic->getName();
      }

      //$url = Url::fromRoute('entity.node.canonical', ['node' => $session->id()])->toString();

      // Check if the node is in the group @TODO optimize
      $sessionsInGroup = \Drupal::entityTypeManager()
        ->getStorage('group_content')
        ->loadByProperties([
          'gid' => $groupId,
          'type' => 'participant-group_node-session',
          'entity_id' => $session->id()
        ]);
      if (!empty($sessionsInGroup)) {
        $suggestedSessions .= "<li>$suggestedSession</li>";
      }
    }

    $form['links'] = [
      '#type' => 'markup',
      '#markup' => $suggestedSessions,
    ];
    $form['#cache'] = ['max-age' => 0];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Don't check pass if declined
    $submitHandler = $form_state->getValues()['op']->getUntranslatedString();

    if ($submitHandler == 'ConfirmDecline') {
      $form_state->clearErrors();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Accept the invitation
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitAccept(array &$form, FormStateInterface $form_state) {

    // User on-boarding
    $this->notice('submitAccept (user:' . (int) $this->user->id() . ')');
    $pass = $form_state->getValue('accept_block')['pass_confirm'];
    $this->user->setPassword($pass)->save();
    user_login_finalize($this->user);
    _user_mail_notify('status_activated', $this->user);

    // Participant status
    speakers_main_update_participant_status('onboarded', $this->user->id());

    $message = $this->t('<p>Thank you for confirming your attendance.</p><p>We would like you to give two lectures during our congress. You may submit up to three lecture proposals for each proposed topic below.</p><p>We encourage you to prepare and plan for your lecture proposal submissions with these criteria in mind in order to provide the highest level of adult continuing education:<br>- Innovative subject matter<br>- Relevant to clinical practice<br>- Basic science research with potential relevance to clinical applications<br>- Suitable for general practitioners<br>- Up-to-date presentation materials with best available scientific evidence<br>- Contemporary clinical case presentations where appropriate</p><p>If you have any questions, do not hesitate to contact us.</p><p>Isabelle Bourzeix, Congress and Education Manager</p>');
    $this->messenger->addMessage($message);

    $form_state->setRedirect('entity.user.canonical', ['user' => $this->user->id()]);
  }

  /**
   * Submit for decline button.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitDecline(array &$form, FormStateInterface $form_state) {
    $this->notice('submitDecline (user:' . (int) $this->user->id() . ')');

    // Change user status to Declined.
    speakers_main_update_participant_status('declined', $this->user->id());

    // Emailing message to Editors.
    $data = [
      'message' => $form_state->getValue('decline_block')['decline_message'],
      'fullName' => $this->user->get('full_name_computed')->getValue(),
      'lang' => $this->languageManager->getCurrentLanguage()->getId()
    ];
    speakers_main_email_message_editor($subject = 'Invitation declined', $data, 'invitation_decline');

    $this->user->block();
    $this->user->save();

    $message = $this->t("<p>Thank you for your reply.</p><p>Your decision to decline our invitation will be forwarded to the FDI Education Committee and the organizing committee of the Association.</p><p>Best regards,<br /><p><b>Isabelle Bourzeix</b>, Congress and Education Manager<br /><b>@email</b></p>", ['@email' => 'ibourzeix@fdiworlddental.org']);
    $this->messenger->addMessage($message);
    $form_state->setRedirect('speakers_main.declined_invitation');
  }

  /**
   * {@inheritdoc}
   *
   * Remove cache
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * Log a notice
   *
   * @param $message
   */
  private function notice($message) {
    \Drupal::logger('speakers_main')->notice($message);
  }

  /**
   * Log a error
   *
   * @param $message
   */
  private function error($message) {
    \Drupal::logger('speakers_main')->error($message);
  }
}
