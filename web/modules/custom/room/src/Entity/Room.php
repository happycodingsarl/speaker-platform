<?php

namespace Drupal\room\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Room entity.
 *
 * @ingroup room
 *
 * @ContentEntityType(
 *   id = "room",
 *   label = @Translation("Room"),
 *   bundle_label = @Translation("Room type"),
 *   handlers = {
 *     "storage" = "Drupal\room\RoomStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\room\RoomListBuilder",
 *     "views_data" = "Drupal\room\Entity\RoomViewsData",
 *     "translation" = "Drupal\room\RoomTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\room\Form\RoomForm",
 *       "add" = "Drupal\room\Form\RoomForm",
 *       "edit" = "Drupal\room\Form\RoomForm",
 *       "delete" = "Drupal\room\Form\RoomDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\room\RoomHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\room\RoomAccessControlHandler",
 *   },
 *   base_table = "room",
 *   data_table = "room_field_data",
 *   revision_table = "room_revision",
 *   revision_data_table = "room_field_revision",
 *   translatable = TRUE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer room entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/room/{room}",
 *     "add-page" = "/admin/structure/room/add",
 *     "add-form" = "/admin/structure/room/add/{room_type}",
 *     "edit-form" = "/admin/structure/room/{room}/edit",
 *     "delete-form" = "/admin/structure/room/{room}/delete",
 *     "version-history" = "/admin/structure/room/{room}/revisions",
 *     "revision" = "/admin/structure/room/{room}/revisions/{room_revision}/view",
 *     "revision_revert" = "/admin/structure/room/{room}/revisions/{room_revision}/revert",
 *     "revision_delete" = "/admin/structure/room/{room}/revisions/{room_revision}/delete",
 *     "translation_revert" = "/admin/structure/room/{room}/revisions/{room_revision}/revert/{langcode}",
 *     "collection" = "/admin/structure/room",
 *   },
 *   bundle_entity_type = "room_type",
 *   field_ui_base_route = "entity.room_type.edit_form"
 * )
 */
class Room extends EditorialContentEntityBase implements RoomInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Room entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Room is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

}
