<?php

namespace Drupal\speakers_export_emails\Plugin\Action;

use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Messenger\MessengerTrait;

/**
 * Action: Export Emails.
 * https://www.drupal.org/docs/8/modules/views-bulk-operations-vbo/creating-a-new-action
 *
 * @Action(
 *   id = "speakers_export_emails_export_emails",
 *   label = @Translation("Export Emails"),
 *   type = "",
 *   confirm = FALSE,
 *   requirements = {
 *     "_permission" = "administer event entities",
 *     "_custom_access" = FALSE,
 *   },
 *   pass_view = TRUE
 * )
 */
class ExportEmails extends ViewsBulkOperationsActionBase {

  use StringTranslationTrait;
  use MessengerTrait;

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    // Do some processing..

    // Don't return anything for a default completion message, otherwise return translatable markup.
    return $this->t('Some result');
  }

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $entities) {
    switch ($this->context['view_id']) {
      case 'session_list':
        $field = 'users_field_data_node__field_participant_uid';
        break;
      case 'participants_list':
        $field = 'users_field_data_group_content_field_data_uid';
        break;
      default:
        $field = FALSE;
    }

    // Export Emails only on these views
    if ($field) {

      // Get Account Ids
      $accountIds = [];
      foreach ($entities as $delta => $entity) {
        if (isset($this->view->result[$delta]->$field)) {
          $accountIds[$this->view->result[$delta]->$field] = $this->view->result[$delta]->$field;
        }
      }

      // Get recipients
      $storage = \Drupal::service('entity_type.manager')->getStorage('user');
      $accounts = $storage->loadMultiple($accountIds);
      $recipients = '';
      foreach ($accounts as $account) {
        $email = $account->getEmail();
        $prefix = $this->getValue($account->get('field_prefix')->getValue());
        $firstName = $this->getValue($account->get('field_first_name')->getValue());
        $lastName =  $this->getValue($account->get('field_last_name')->getValue());
        $recipients .= "$prefix $firstName $lastName <$email>;";
      }

      //$this->messenger()->addStatus('Please copy paste the following recipients in your email:');
      $this->messenger()->addMessage($this->t('@r', [
        '@r' => $recipients,
      ]), 'clipboard');
    }
    else{
      $this->messenger()->addWarning('Nothing done.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($object->getEntityType() === 'node') {
      $access = $object->access('update', $account, TRUE)
        ->andIf($object->status->access('edit', $account, TRUE));
      return $return_as_object ? $access : $access->isAllowed();
    }

    // Other entity types may have different
    // access methods and properties.
    return TRUE;
  }

  /**
   * Get Field value
   *
   * @param $field
   *
   * @return bool
   */
  private function getValue($field) {
    $value = FALSE;

    if (is_array($field)) {
      $field = reset($field);
      if (isset($field['value'])) {
        $value = $field['value'];
      }
    }

    return $value;
  }
}
