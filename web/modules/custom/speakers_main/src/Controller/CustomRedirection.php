<?php

namespace Drupal\speakers_main\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the dashboard demo.
 */
class CustomRedirection extends ControllerBase {

  /**
   * Redirect the user to profile edit.
   *
   * @return array
   *   A simple renderable array.
   */
  public function EditMyProfile() {
    $uid = \Drupal::currentUser()->id();

    return $this->redirect('entity.user.edit_form', ['user' => $uid]);
  }

  /**
   * Redirect the user to user page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function MyDashboard() {
    $uid = \Drupal::currentUser()->id();

    return $this->redirect('user.page');
  }
}
