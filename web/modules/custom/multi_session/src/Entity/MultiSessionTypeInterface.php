<?php

namespace Drupal\multi_session\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Multi session type entities.
 */
interface MultiSessionTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
