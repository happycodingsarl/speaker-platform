<?php

namespace Drupal\speakers_main\Plugin\Action;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsPreconfigurationInterface;

/**
 * Action: Change Speaker Status.
 * https://www.drupal.org/docs/8/modules/views-bulk-operations-vbo/creating-a-new-action
 *
 * @Action(
 *   id = "speakers_main_change_speaker_status",
 *   label = @Translation("Change Speaker Status"),
 *   type = "",
 *   confirm = FALSE,
 *   requirements = {
 *     "_permission" = "administer event entities",
 *     "_custom_access" = FALSE,
 *   },
 *   pass_view = TRUE
 * )
 */
class ChangeSpeakerStatus extends ViewsBulkOperationsActionBase {

  use StringTranslationTrait;
  use MessengerTrait;

  /**
   * Configuration form builder.
   *
   * If this method has implementation, the action is
   * considered to be configurable.
   *
   * @param array $form
   *   Form array.
   * @param FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The configuration form.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['speakers_main_config_status'] = [
      '#type' => 'select',
      '#title' => $this->t('Choose the status'),
      '#default_value' => 'onboarded',
      '#options' => ['onboarded' => 'onboarded', 'declined' => 'Declined'],
      '#description' => 'The chosen status will be set to all selected speakers',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    // Do some processing..

    // Don't return anything for a default completion message, otherwise return translatable markup.
    return $this->t('Some result');
  }

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $entities) {
    $ok = $ko = FALSE;

    // Change Status only on this view
    if ($this->context['view_id'] == 'participants_list') {

      // Update statuses
      $newStatus = $this->configuration['speakers_main_config_status'];
      foreach ($entities as $delta => $entity) {
        if (($oldStatus = $entity->field_member_status->value) && (in_array($oldStatus, [
            'invited',
            'onboarded',
            'declined'
          ]))) {

          // Save new Status
          $entity->set('field_member_status', $newStatus);
          $entity->save();

          if (!$ok) {
            $ok = $this->t('The status has been changed to "@n" on these speakers: ', [
              '@n' => $newStatus
            ]);
          }
          $ok .= $this->t('@t(@n), ', [
            '@t' => $entity->label->value,
            '@n' => $entity->id()
          ]);

        }
        else {
          if (!$ko) {
            $ko = $this->t('The old status should be: invited, onboarded or declined, so status cannot be changed on these speakers: ');
          }
          $ko .= $this->t('@t(@n), ', [
            '@t' => $entity->label->value,
            '@n' => $entity->id()
          ]);
        }
      }
    }
    else {
      $ko = 'Nothing done.';
    }

    // Display messages
    if ($ok) {
      $this->messenger()->addStatus($ok);
    }
    if ($ko) {
      $this->messenger()->addWarning($ko);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($object->getEntityType() === 'node') {
      $access = $object->access('update', $account, TRUE)
        ->andIf($object->status->access('edit', $account, TRUE));
      return $return_as_object ? $access : $access->isAllowed();
    }

    // Other entity types may have different
    // access methods and properties.
    return TRUE;
  }
}
