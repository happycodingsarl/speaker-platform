<?php

/**
 * @file
 * Views hook implementations.
 */

/**
 * Implements hook_views_data_alter().
 */
function views_config_var_views_data_alter(&$data) {
  $data['views']['custom_var'] = [
    'title' => t('Custom Var'),
    'help' => t("Custom Var"),
    'field' => [
      'id' => 'custom_var',
    ],
  ];
}
