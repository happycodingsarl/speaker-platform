<?php

namespace Drupal\room;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\room\Entity\RoomInterface;

/**
 * Defines the storage handler class for Room entities.
 *
 * This extends the base storage class, adding required special handling for
 * Room entities.
 *
 * @ingroup room
 */
class RoomStorage extends SqlContentEntityStorage implements RoomStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(RoomInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {room_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {room_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(RoomInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {room_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('room_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
