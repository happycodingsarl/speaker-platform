<?php

namespace Drupal\break_time_slot\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Break time slot entity.
 *
 * @ingroup break_time_slot
 *
 * @ContentEntityType(
 *   id = "break_time_slot",
 *   label = @Translation("Break time slot"),
 *   handlers = {
 *     "storage" = "Drupal\break_time_slot\BreakTimeSlotStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\break_time_slot\BreakTimeSlotListBuilder",
 *     "views_data" = "Drupal\break_time_slot\Entity\BreakTimeSlotViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\break_time_slot\Form\BreakTimeSlotForm",
 *       "add" = "Drupal\break_time_slot\Form\BreakTimeSlotForm",
 *       "edit" = "Drupal\break_time_slot\Form\BreakTimeSlotForm",
 *       "delete" = "Drupal\break_time_slot\Form\BreakTimeSlotDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\break_time_slot\BreakTimeSlotHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\break_time_slot\BreakTimeSlotAccessControlHandler",
 *   },
 *   base_table = "break_time_slot",
 *   revision_table = "break_time_slot_revision",
 *   revision_data_table = "break_time_slot_field_revision",
 *   translatable = FALSE,
 *   admin_permission = "administer break time slot entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/break_time_slot/{break_time_slot}",
 *     "add-form" = "/admin/structure/break_time_slot/add",
 *     "edit-form" = "/admin/structure/break_time_slot/{break_time_slot}/edit",
 *     "delete-form" = "/admin/structure/break_time_slot/{break_time_slot}/delete",
 *     "version-history" = "/admin/structure/break_time_slot/{break_time_slot}/revisions",
 *     "revision" = "/admin/structure/break_time_slot/{break_time_slot}/revisions/{break_time_slot_revision}/view",
 *     "revision_revert" = "/admin/structure/break_time_slot/{break_time_slot}/revisions/{break_time_slot_revision}/revert",
 *     "revision_delete" = "/admin/structure/break_time_slot/{break_time_slot}/revisions/{break_time_slot_revision}/delete",
 *     "collection" = "/admin/structure/break_time_slot",
 *   },
 *   field_ui_base_route = "break_time_slot.settings"
 * )
 */
class BreakTimeSlot extends EditorialContentEntityBase implements BreakTimeSlotInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    // If no revision author has been set explicitly,
    // make the break_time_slot owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Break time slot entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Break time slot entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Break time slot is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
