<?php

namespace Drupal\speakers_civicrm\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Defines a form that configures speakers civiCRM settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'speakers_civicrm_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'speakers_civicrm.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $current_url = Url::createFromRequest($request);
    $speakers_civicrm_config = $this->config('speakers_civicrm.settings');

    $form['speakers_civicrm_api_url'] = [
      '#type' => 'textfield',
      '#title' => t('API URL'),
      '#default_value' => $speakers_civicrm_config->get('speakers_civicrm_api_url'),
      '#description' => t('API URL'),
    ];
    $form['speakers_civicrm_api_key'] = [
      '#type' => 'textfield',
      '#title' => t('API KEY'),
      '#default_value' => $speakers_civicrm_config->get('speakers_civicrm_api_key'),
      '#description' => t('API KEY'),
    ];
    $form['speakers_invitation_token'] = [
      '#type' => 'textfield',
      '#title' => t('Invitation token'),
      '#default_value' => $speakers_civicrm_config->get('speakers_invitation_token'),
      '#description' => t('The invitation token'),
    ];
    $form['speakers_cron_period'] = [
      '#type' => 'textfield',
      '#title' => t('Cron period'),
      '#default_value' => $speakers_civicrm_config->get('speakers_cron_period'),
      '#description' => t('The cron period in seconds'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('speakers_civicrm.settings')
      ->set('speakers_civicrm_api_url', $values['speakers_civicrm_api_url'])
      ->set('speakers_civicrm_api_key', $values['speakers_civicrm_api_key'])
      ->set('speakers_invitation_token', $values['speakers_invitation_token'])
      ->set('speakers_cron_period', $values['speakers_cron_period'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
