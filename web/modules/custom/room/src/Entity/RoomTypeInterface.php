<?php

namespace Drupal\room\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Room type entities.
 */
interface RoomTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
