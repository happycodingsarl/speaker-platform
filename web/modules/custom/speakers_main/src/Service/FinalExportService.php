<?php

namespace Drupal\speakers_main\Service;

use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\room\Entity\Room;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Data layer.
 *
 * see https://www.drupal.org/docs/8/api/database-api
 */
class FinalExportService {

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * @var SQL Query
   */
  private $query;

  /**
   * @param \Drupal\Core\Database\Connection $connection
   *   A Database connection to use for reading and writing configuration data.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
    $this->setQuery();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('database')
    );
  }

  /**
   * @TODO manage group
   */
  private function setQuery() {
    $this->query = "
SELECT multi.uuid, multi.type, multi.id, min(msd.field_scheduled_date_value) as startDate, max(field_scheduled_date_end_value) as endDate, c.field_code_value as codeRoom
FROM {multi_session} as multi
LEFT JOIN {multi_session__field_session} fs ON fs.entity_id = multi.id AND fs.bundle='default'
LEFT JOIN {node__field_scheduled_date} msd ON fs.field_session_target_id = msd.entity_id AND msd.bundle='session'
LEFT JOIN {node__field_room} r ON fs.field_session_target_id = r.entity_id AND r.bundle='session'
LEFT JOIN {room__field_code} c ON r.field_room_target_id = c.entity_id AND c.bundle='default'
GROUP BY multi.uuid
UNION
SELECT n.uuid, n.type, n.nid, sd.field_scheduled_date_value, sd.field_scheduled_date_end_value, c.field_code_value as codeRoom
FROM {node} as n
LEFT JOIN {node_field_data} nfd ON n.nid = nfd.nid
LEFT JOIN {node__field_scheduled_date} sd ON n.nid = sd.entity_id AND sd.bundle='session'
LEFT JOIN {node__field_room} r ON n.nid = r.entity_id AND r.bundle='session'
LEFT JOIN {room__field_code} c ON r.field_room_target_id = c.entity_id AND c.bundle='default'
WHERE
sd.field_scheduled_date_value IS NOT NULL
AND
n.nid NOT IN (
SELECT field_session_target_id
FROM {multi_session__field_session}
ORDER BY field_session_target_id
)
AND
nfd.status = 1";
  }

  /**
   * Get list of sessions & multiple session order by start date ASC & end date DESC
   *
   * @return mixed
   */
  public function getFirstLevel() {
    /**
     * Error with "GROUP BY sql" command by default
     *
     * see:https://www.drupal.org/project/drupal/issues/2989922#comment-13060134
     */
    $this->connection->query("SET SESSION sql_mode = ''")->execute();

    $query = $this->connection->query($this->query . " ORDER BY startDate ASC, endDate DESC, cast(codeRoom as unsigned);");

    return $query->fetchAll();
  }

  /**
   * Get list of codeTime
   *
   * @return array
   */
  public function getCodeTime() {
    static $codeTimes = null;

    if ($codeTimes === null) {

      $this->connection->query("SET SESSION sql_mode = ''")->execute();
      $query = $this->connection->query("
SELECT multi.uuid, multi.type, multi.id as id, TIME_FORMAT(STR_TO_DATE(min(msd.field_scheduled_date_value),'%Y-%m-%dT%H:%i:%s'), '%H:%i') as startDate,
TIME_FORMAT(STR_TO_DATE(max(msd.field_scheduled_date_end_value),'%Y-%m-%dT%H:%i:%s'), '%H:%i') as endDate,
min(msd.field_scheduled_date_value) as fullStartDate, max(msd.field_scheduled_date_end_value) as fullEndDate
FROM {multi_session} as multi
LEFT JOIN {multi_session__field_session} fs ON fs.entity_id = multi.id AND fs.bundle = 'default'
LEFT JOIN {node__field_scheduled_date} msd ON fs.field_session_target_id = msd.entity_id AND msd.bundle='session'
GROUP BY multi.uuid
UNION
SELECT n.uuid, n.type, n.nid, TIME_FORMAT(STR_TO_DATE(sd.field_scheduled_date_value,'%Y-%m-%dT%H:%i:%s'), '%H:%i'),
TIME_FORMAT(STR_TO_DATE(sd.field_scheduled_date_end_value,'%Y-%m-%dT%H:%i:%s'), '%H:%i'), sd.field_scheduled_date_value, sd.field_scheduled_date_end_value
FROM {node} as n
LEFT JOIN {node_field_data} nfd ON n.nid = nfd.nid
LEFT JOIN {node__field_scheduled_date} sd ON n.nid = sd.entity_id AND sd.bundle='session'
WHERE
sd.field_scheduled_date_value IS NOT NULL
AND
n.nid NOT IN (
SELECT field_session_target_id
FROM {multi_session__field_session}
ORDER BY field_session_target_id
)
AND
nfd.status = 1
ORDER BY startDate ASC, endDate DESC;");

      $timeCodes = $query->fetchAll();

      $lastRecord = '';
      $codeTimes = [];
      $codeASCII = ord('A');

      foreach ($timeCodes as $entity) {

        // Remove duplicates
        if ($lastRecord != $entity->startDate . $entity->endDate) {

          // Format dates
          $startDate = new DrupalDateTime($entity->fullStartDate, DateTimeItemInterface::STORAGE_TIMEZONE);
          $startDateFormatted = $startDate->format('H:i', ['timezone' => drupal_get_user_timezone()]);

          $endDate = new DrupalDateTime($entity->fullEndDate, DateTimeItemInterface::STORAGE_TIMEZONE);
          $endDateFormatted = $endDate->format('H:i', ['timezone' => drupal_get_user_timezone()]);

          $codeTimes[$startDateFormatted . $endDateFormatted] = [
            'code' => chr($codeASCII),
            'startDate' => $startDateFormatted,
            'endDate' => $endDateFormatted
          ];
          $codeASCII++;
        }
        $lastRecord = $entity->startDate . $entity->endDate;
      }
    }

    return $codeTimes;
  }

  /**
   * Get participants list
   *
   * @return mixed
   */
  public function participants() {

    // Get current group
    $groupId = \Drupal::service('speakers.group')->getEventGroupId();

    $this->connection->query("SET SESSION sql_mode = ''")->execute();

    $query = $this->connection
      ->query("
        SELECT
          p.field_participant_target_id as uid,
          uflname.field_last_name_value as ulname,
          uffname.field_first_name_value as ufname,
          ufbio.field_biography_value as ubio
        FROM
          {node__field_participant} as p
        LEFT JOIN
          {group_content_field_data} as gc
          ON p.entity_id = gc.entity_id
          AND gc.type = 'participant-group_node-session'
        LEFT JOIN
          {node__field_scheduled_date} as sd
          ON p.entity_id = sd.entity_id
          AND sd.bundle = 'session'
        LEFT JOIN
          {user__field_last_name} as uflname
          ON p.field_participant_target_id = uflname.entity_id
        LEFT JOIN
          {user__field_first_name} as uffname
          ON p.field_participant_target_id = uffname.entity_id
        LEFT JOIN
          {user__field_biography} as ufbio
          ON p.field_participant_target_id = ufbio.entity_id
        WHERE
          sd.field_scheduled_date_value IS NOT NULL
          AND gc.gid = $groupId
        GROUP BY uid
        ORDER BY ulname
      ");
    return $query->fetchAll();
  }

  /**
   * Generate Days code from the event dates
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getDaysCode() {
    $daysCode = [];

    $eventId = speakers_main_get_event_id();
    if (!is_null($eventId)) {
      $event = \Drupal::entityTypeManager()
        ->getStorage('event')
        ->load($eventId);

      $date = \DateTime::createFromFormat('Y-m-d', $event->field_start_and_end_dates->value);
      $endDate = \DateTime::createFromFormat('Y-m-d', $event->field_start_and_end_dates->end_value);
      $code = 1;

      while ($date <= $endDate) {
        $daysCode[$date->format('Y-m-d')] = [
          'code' => $code,
          'label' => $date->format('M d')
        ];
        $code++;
        $date = $date->modify('+1 day');
      }
    }

    return $daysCode;
  }

  /**
   * @return array
   */
  public function getCodeRooms() {
    static $codeRooms = null;

    if ($codeRooms === null) {
      $roomCode = $roomCodeSorted = [];

      $query = \Drupal::entityQuery('room')
        ->sort('field_code', 'ASC');
      $roomIds = $query->execute();
      $roomEntities = Room::loadMultiple($roomIds);

      foreach ($roomEntities as $roomEntity) {
        $roomCode[$roomEntity->id()] = [
          'code' => $roomEntity->field_code->value,
          'label' => $roomEntity->name->value
        ];
        $roomCodeSorted[(int) $roomEntity->field_code->value] = [
          'code' => $roomEntity->field_code->value,
          'label' => $roomEntity->name->value
        ];
      }
      sort($roomCodeSorted);
      $codeRooms = [$roomCode, $roomCodeSorted];
    }

    return $codeRooms;
  }

  /**
   * Calculate the code
   *
   * @param $entityId
   * @param $startDate
   * @param $endDate
   * @param $roomId
   * @return string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function calculateCode($entityId, $startDate, $endDate, $roomId) {
    $timeCodes = $this->getCodeTime();
    $daysCode = $this->getDaysCode();
    list($rooms, $roomsSorted) = $this->getCodeRooms();

    // Get Day Code
    $day = explode('T', $startDate);
    if (isset($daysCode[$day[0]]['code'])) {
      $dayCode = $daysCode[$day[0]]['code'];
    }
    else {
      $dayCode = '#';
      \Drupal::messenger()
        ->addWarning('Day not found on session (' . $entityId . ')');
    }

    // Get Time Code
    $dateTimeStartDate = new DrupalDateTime($startDate, DateTimeItemInterface::STORAGE_TIMEZONE);
    $startDateFormatted = $dateTimeStartDate->format('H:i', ['timezone' => drupal_get_user_timezone()]);
    $dateTimeEndDate = new DrupalDateTime($endDate, DateTimeItemInterface::STORAGE_TIMEZONE);
    $endDateFormatted = $dateTimeEndDate->format('H:i', ['timezone' => drupal_get_user_timezone()]);

    if (isset($timeCodes[$startDateFormatted . $endDateFormatted]['code'])) {
      $timeCode = $timeCodes[$startDateFormatted . $endDateFormatted]['code'];
    }
    else {
      $timeCode = '#';
      \Drupal::messenger()
        ->addWarning('Time not found on session (' . $entityId . ')');
    }

    // Get Room Code
    if (isset($rooms[$roomId]['code'])) {
      $roomCode = $rooms[$roomId]['code'];
    }
    else {
      $roomCode = '#';
      \Drupal::messenger()
        ->addWarning('Room not found on session (' . $entityId . ')');
    }

    return $dayCode . $timeCode . $roomCode;
  }

  /**
   * Store & retrieve the speakers codes with State API
   *
   * @param array $scheduledSpeakers
   * @return array|bool
   */
  public function speakerCodes($scheduledSpeakers = []) {
    if (empty($scheduledSpeakers)) {

      return \Drupal::state()->get('speakers_code');
    }
    else {
      \Drupal::state()->set('speakers_code', $scheduledSpeakers);

      return TRUE;
    }
  }

  /**
   * Store & retrieve the codes  by keyword with State API
   *
   * @param array $keywordsList
   * @return array|bool
   */
  public function codesByKeyword($keywordsList = []) {
    if (empty($keywordsList)) {

      return \Drupal::state()->get('codes_by_keywords');
    }
    else {
      \Drupal::state()->set('codes_by_keywords', $keywordsList);

      return TRUE;
    }
  }

  /**
   * Store & retrieve the periods
   * Morning session 9:00
   * Midday session 11:30
   * Afternoon session 12:30
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function periods() {
    static $periods = null;

    if ($periods === null) {
      $periods = [];

      $eventId = speakers_main_get_event_id();
      if (!is_null($eventId)) {
        $event = \Drupal::entityTypeManager()
          ->getStorage('event')
          ->load($eventId);

        $periods = [
          $event->field_afternoon_sessions_start->value => 'Afternoon sessions',
          $event->field_midday_sessions_start->value => 'Midday sessions',
          $event->field_morning_sessions_start->value => 'Morning sessions',
        ];
      }
    }

    return $periods;
  }
}
