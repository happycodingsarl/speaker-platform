<?php

namespace Drupal\room\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;
use Drupal\room\Entity\RoomInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class RoomController.
 *
 *  Returns responses for Room routes.
 */
class RoomController extends ControllerBase implements ContainerInjectionInterface {


  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Constructs a new RoomController.
   *
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   */
  public function __construct(DateFormatter $date_formatter, Renderer $renderer) {
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * Displays a Room revision.
   *
   * @param int $room_revision
   *   The Room revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($room_revision) {
    $room = $this->entityTypeManager()->getStorage('room')
      ->loadRevision($room_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('room');

    return $view_builder->view($room);
  }

  /**
   * Page title callback for a Room revision.
   *
   * @param int $room_revision
   *   The Room revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($room_revision) {
    $room = $this->entityTypeManager()->getStorage('room')
      ->loadRevision($room_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $room->label(),
      '%date' => $this->dateFormatter->format($room->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Room.
   *
   * @param \Drupal\room\Entity\RoomInterface $room
   *   A Room object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(RoomInterface $room) {
    $account = $this->currentUser();
    $room_storage = $this->entityTypeManager()->getStorage('room');

    $langcode = $room->language()->getId();
    $langname = $room->language()->getName();
    $languages = $room->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $room->label()]) : $this->t('Revisions for %title', ['%title' => $room->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all room revisions") || $account->hasPermission('administer room entities')));
    $delete_permission = (($account->hasPermission("delete all room revisions") || $account->hasPermission('administer room entities')));

    $rows = [];

    $vids = $room_storage->revisionIds($room);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\room\RoomInterface $revision */
      $revision = $room_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $room->getRevisionId()) {
          $link = $this->l($date, new Url('entity.room.revision', [
            'room' => $room->id(),
            'room_revision' => $vid,
          ]));
        }
        else {
          $link = $room->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.room.translation_revert', [
                'room' => $room->id(),
                'room_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.room.revision_revert', [
                'room' => $room->id(),
                'room_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.room.revision_delete', [
                'room' => $room->id(),
                'room_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['room_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
