<?php

namespace Drupal\break_time_slot;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Break time slot entity.
 *
 * @see \Drupal\break_time_slot\Entity\BreakTimeSlot.
 */
class BreakTimeSlotAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\break_time_slot\Entity\BreakTimeSlotInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished break time slot entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published break time slot entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit break time slot entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete break time slot entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add break time slot entities');
  }


}
