<?php

namespace Drupal\multi_session;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\multi_session\Entity\MultiSessionInterface;

/**
 * Defines the storage handler class for Multi session entities.
 *
 * This extends the base storage class, adding required special handling for
 * Multi session entities.
 *
 * @ingroup multi_session
 */
interface MultiSessionStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Multi session revision IDs for a specific Multi session.
   *
   * @param \Drupal\multi_session\Entity\MultiSessionInterface $entity
   *   The Multi session entity.
   *
   * @return int[]
   *   Multi session revision IDs (in ascending order).
   */
  public function revisionIds(MultiSessionInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Multi session author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Multi session revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\multi_session\Entity\MultiSessionInterface $entity
   *   The Multi session entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(MultiSessionInterface $entity);

  /**
   * Unsets the language for all Multi session with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
