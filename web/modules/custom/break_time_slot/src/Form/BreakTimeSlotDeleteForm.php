<?php

namespace Drupal\break_time_slot\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Break time slot entities.
 *
 * @ingroup break_time_slot
 */
class BreakTimeSlotDeleteForm extends ContentEntityDeleteForm {


}
