<?php

namespace Drupal\room\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Room entities.
 *
 * @ingroup room
 */
class RoomDeleteForm extends ContentEntityDeleteForm {


}
