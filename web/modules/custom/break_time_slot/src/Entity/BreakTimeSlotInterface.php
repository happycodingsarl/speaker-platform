<?php

namespace Drupal\break_time_slot\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Break time slot entities.
 *
 * @ingroup break_time_slot
 */
interface BreakTimeSlotInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Break time slot name.
   *
   * @return string
   *   Name of the Break time slot.
   */
  public function getName();

  /**
   * Sets the Break time slot name.
   *
   * @param string $name
   *   The Break time slot name.
   *
   * @return \Drupal\break_time_slot\Entity\BreakTimeSlotInterface
   *   The called Break time slot entity.
   */
  public function setName($name);

  /**
   * Gets the Break time slot creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Break time slot.
   */
  public function getCreatedTime();

  /**
   * Sets the Break time slot creation timestamp.
   *
   * @param int $timestamp
   *   The Break time slot creation timestamp.
   *
   * @return \Drupal\break_time_slot\Entity\BreakTimeSlotInterface
   *   The called Break time slot entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Break time slot revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Break time slot revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\break_time_slot\Entity\BreakTimeSlotInterface
   *   The called Break time slot entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Break time slot revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Break time slot revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\break_time_slot\Entity\BreakTimeSlotInterface
   *   The called Break time slot entity.
   */
  public function setRevisionUserId($uid);

}
