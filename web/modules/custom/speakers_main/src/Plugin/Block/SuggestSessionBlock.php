<?php

namespace Drupal\speakers_main\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Provides a 'SuggestSessionBlock' block.
 *
 * @Block(
 *  id = "suggest_session_block",
 *  admin_label = @Translation("Suggest Session block"),
 * )
 */
class SuggestSessionBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity manager
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Creates a SuggestSession instance.
   *
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function build() {
//    speakers_main_get_event_id();//@TODO remove
    $result = [];
    $output = [];
    $status = [];
    $i = 0;
    $topic_is_empty = FALSE;

    // Get current group
    $groupId = \Drupal::service('speakers.group')->getEventGroupId();

    // Get current user id or from url (this block is only displayed on dashboard) @TODO review
    $uid = \Drupal::currentUser()->id();
    $path = \Drupal::request()->getPathInfo();
    $pathArgs = explode("/", $path);
    if (!empty($pathArgs) && is_array($pathArgs)) {
      $uid = array_pop($pathArgs);
    }

    if (!empty(intval($uid))) {
      $participantSessions = \Drupal::entityTypeManager()
        ->getStorage('node')
        ->loadByProperties(['field_participant' => $uid]);

      // Check if the node is in the group @TODO optimize
      foreach ($participantSessions as $participantSession) {
        $sessionsInGroup = \Drupal::entityTypeManager()
          ->getStorage('group_content')
          ->loadByProperties([
            'gid' => $groupId,
            'type' => 'participant-group_node-session',
            'entity_id' => $participantSession->id()
          ]);
        if (!empty($sessionsInGroup)) {
          $sessions[] = $participantSession;
        }
      }

      if (!empty($sessions)) {
        foreach ($sessions as $key => $session) {

          // Get session status to hide SuggestSession item if we have 3 proposed Sessions by Speakers.
          $session_status = speakers_session_get_status($session->id());

          // Get category name
          $category = $session->get('field_session_category')
            ->referencedEntities();
          $category_name = '';
          if (!empty($category) && !empty(reset($category))) {
            $category_name = reset($category)->getName();
          }

          // Get topic name
          $topic = $session->get('field_session_topic')->referencedEntities();
          $topic_name = '';
          if (!empty($topic) && !empty(reset($topic))) {
            $topic_name = reset($topic)->getName();
          }

          // Save only suggested session for block.
          if (in_array($session_status, ['suggested'])) {
            $result[$category_name . '-' . $topic_name][$key]['category'] = $category_name;
            $result[$category_name . '-' . $topic_name][$key]['topic'] = $topic_name;
            $topic_is_empty = empty($topic_name) ? TRUE : FALSE;
          }

          // Save all sessions status for calculate proposed by Speaker.
          if (in_array($session_status, ['proposed', 'accepted', 'declined'])) {
            $topic_name = $topic_is_empty ? '' : $topic_name;
            $status[$category_name . '-' . $topic_name][$key] = $session_status;
          }
        }

        foreach ($result as $key => $item) {
          // Set options for URL.
          $s_category = reset($item)['category'];
          $s_topic = reset($item)['topic'];

          // Set parameters for URL:
          // "/node/add/session/{participant_id}/{s_category}/{s_topic}".
          $options = [
            'node_type' => $session->type->target_id,
            'pid' => $uid,
            's_category' => $s_category,
            's_topic' => $s_topic,
            'destination' => Url::fromRoute('<current>')->toString(),
          ];

          // Hide box if we have already 3 proposed Sessions by Speakers.
          if (isset($status[$key]) && is_array($status[$key])) {
            if (count($status[$key]) < 3) {
              $output[$i]['url'] = Url::fromRoute('node.add', $options);
              $output[$i]['category'] = $s_category;
              $output[$i]['topic'] = $s_topic;
              $i++;
            }
          }
          else {
            $output[$i]['url'] = Url::fromRoute('node.add', $options);
            $output[$i]['category'] = $s_category;
            $output[$i]['topic'] = $s_topic;
            $i++;
          }
        }
      }
    }
    $build = [
      '#theme' => 'suggest_session_block',
      '#results' => $output,
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }
}
