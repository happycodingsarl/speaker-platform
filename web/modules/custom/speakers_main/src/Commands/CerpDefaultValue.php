<?php

namespace Drupal\speakers_main\Commands;

use Drupal\node\Entity\Node;
use Drush\Commands\DrushCommands;

/**
 * A Drush command file.
 * see: https://www.axelerant.com/resources/team-blog/how-to-write-custom-drush-9-commands-for-drupal-8
 *
 */
class CerpDefaultValue extends DrushCommands {
  /**
   * Set the default value for field_session_cerp.
   *
   * @command speakers_main:cerp_default
   * @aliases c-d
   * @usage cerp:cerp_default
   */
  public function cerp_default() {
    $this->output()->writeln('Set the default value for field_session_cerp');

    $sessionIds = \Drupal::entityQuery('node')
      ->condition('type', 'session')
      ->execute();
    $this->output()->writeln(json_encode($sessionIds) . ' !');
    $sessions = Node::loadMultiple($sessionIds);

    foreach ($sessions as $session) {
      $session->set('field_session_cerp', 0,01);
      //$session->save();

      $this->output()->writeln($session->id() . 'updated !');
    }

    $this->output()->writeln('Set the default value for field_session_cerp done!');
  }
}
