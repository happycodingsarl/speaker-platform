<?php

namespace Drupal\speakers_civicrm\Controller;

use Drupal\speakers_civicrm\Connection;
use Drupal\user\UserInterface;
use Drupal\user\Entity\User;
use Drupal\event\Entity\EventInterface;
use Drupal\group\Entity\GroupContent;
use Drupal\group\Entity\Group;
use \Drupal\taxonomy\Entity\Term;

/**
 */
class CiviCRMController {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The CiviCRM event Id.
   */
  protected $CiviEventId;

  /**
   * The Drupal event Id.
   */
  protected $eventId;

  /**
   * Constructs a new Controller.
   *
   */
  public function __construct() {

  }


  /**
   * When saving an event => Import event information from CiviCRM
   */
  public function getEventFromCivi(EventInterface $event) {

    // Search the CiviCRM Event Id
    $CiviEventId = $this->getCivicrmId($event);
    $this->log("Event sync started (CiviId:$CiviEventId)");

    if ($CiviEventId) {
      $this->CiviEventId = $CiviEventId;
      //$this->eventId = $event->id;

      // Get event information
      $connection = new Connection();
      $data = $connection->queryEndpoint('event', ['id' => $CiviEventId]);

      // Response check
      if (!empty($data->title)) {

        // Event name
        $event->setName(substr($data->title, 0, 128));

        // Event date
        if (!empty($data->start_date) && !empty($data->end_date)) {
          $date = $event->get('field_start_and_end_dates');

          $values = [
            [
              'value' => $this->convertCiviDateToSpeakersDate($data->start_date),
              'end_value' => $this->convertCiviDateToSpeakersDate($data->end_date),
            ],
          ];
          $date->setValue($values);
        }

        // Event fields
        $mapping = [
          'street_address' => 'field_street_address',
          'supplemental_address_1' => 'field_street_address_1',
          'supplemental_address_2' => 'field_street_address_2',
          'supplemental_address_3' => 'field_street_address_3',
          'postal_code' => 'field_zip_code',
          'city' => 'field_city',
          'name' => 'field_country',
        ];
        foreach ($mapping as $civiField => $eventField) {
          (!empty($data->$civiField)) ? $event->set($eventField, $data->$civiField) : $event->set($eventField, '');
        }
        $this->log("Event sync finished");
      }
    }
  }

  public function getEventUpdates() {

  }

  public function getContactUpdates() {

  }

  public function getContacts($contactIds = []) {
    $connection = new Connection();
    $connection->method = 'POST';

    return $connection->queryEndpoint('contacts', ['contact_id' => $contactIds]);
  }

  /**
   * Get event information
   *
   * @param string $CiviEventId
   *
   * @return object
   */
  public function getEventParticipantsList($CiviEventId = '') {
    $connection = new Connection();

    return $connection->queryEndpoint('participants', ['id' => $CiviEventId]);
  }

  /**
   * Update status on CiviCRM
   *
   * @param int $eventId
   * @param int $contactId
   * @param string $status
   *
   * @return object
   */
  public function updateStatus($eventId = 0, $contactId = 0, $status = '') {
    $connection = new Connection();
    $connection->method = 'POST';

    return $connection->queryEndpoint('participant', ['event_id' => $eventId, 'contact_id' => $contactId, 'status' => $status]);
  }

  /**
   * Get Keywords from Drupal Website
   *
   * @return object
   */
  private function getKeywords() {
    $connection = new Connection();
    $connection->method = 'POST';

    return $connection->queryEndpoint('taxonomy', ['vocabulary' => 'keywords']);
  }

  /**
   * Update Keywords from Drupal Website
   */
  public function updateKeywords() {

    //$this->log("Keywords sync started");

    // Get the remote Keywords
    $remoteKeywords = (array) $this->getKeywords();
    if (empty($remoteKeywords)) {
      $this->log("The service returns nothing for keywords.");
      return;
    }
    $remoteKeywords = array_values($remoteKeywords);
    $remoteKeywordsUuid = array_column($remoteKeywords, 'uuid');

    // Load the Keywords terms.
    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', "keywords");
    $tids = $query->execute();
    $keywords = Term::loadMultiple($tids);

    $updated = $removed = $unchanged = 0;

    foreach ($keywords as $term) {
      //$this->log("term => " . $term->getName() . ':' . $term->id() . ':' . $term->uuid()  . ':' . $term->get('status')->value);

      $key = array_search($term->uuid(), $remoteKeywordsUuid);
      if ($key !== false) {
        if ($remoteKeywords[$key]->name != $term->getName()) {

          // Update from remote
          $term = Term::load($term->id());
          $term->setName($remoteKeywords[$key]->name);
          $term->Save();
          $updated++;
          //$this->log("exist in remote. update the term with the array key:" . $key);
        }
        else{

          // Unchanged
          $unchanged++;
          //$this->log("exist in remote. nothing change on the term with the array key:" . $key);
        }

        // Remove keyword from $remoteKeywords
        unset($remoteKeywords[$key]);
      }
      else {
        if ($term->get('status')->value == 1) {

          // Depublish keyword
          $term = Term::load($term->id());
          $term->status->setValue(0);
          $term->Save();
          //$this->log("doesn't exist in remote");

          $removed++;
        }
        else{

          // Unchanged
          $unchanged++;
        }
      }
    }

    // Create new Keywords
    foreach ($remoteKeywords as $remoteKeyword) {
      $term = Term::create([
        'vid' => 'keywords',
        'name' => $remoteKeyword->name,
        'uuid' => $remoteKeyword->uuid,
      ]);
      $term->save();
    }

    $this->log('Keywords sync => added: ' . count($remoteKeywords) . ' / updated: ' . $updated  . ' / unchanged: ' . $unchanged . ' / removed: ' . $removed);
  }

  /**
   * Update members from Civi participants
   *
   * @param int $id
   */
  public function checkEventParticipantsUpdates($id = 0) {

    // Get participants from civi event
    $event = \Drupal::entityTypeManager()->getStorage('event')->load($id);
    $CiviEventId = $this->getCivicrmId($event);
    //$this->log("Participants sync started on event $id (CiviId:$CiviEventId)");
    $participants = (array) $this->getEventParticipantsList($CiviEventId);
    if (empty($participants)) {
      $this->log("The service returns nothing for participants.");
      return;
    }

    // Get Group members
    $groupContents = GroupContent::loadByEntity($event);
    unset($event);
    if (!is_array($groupContents) || empty($groupContents)) {
      $this->log("The group was not found.");
      return;
    }
    $groupContent = reset($groupContents);
    unset($groupContents);
    $group = $groupContent->getGroup();
    unset($groupContent);
    $members = $group->getMembers();

    // Check which action should be done on a participant
    $usersToSync = $groupMembersToRemove = [];
    $participantsExternalIds = array_keys($participants);
    $participantsToAdd = array_combine(array_keys($participants), array_keys($participants));
    foreach ($members as $member) {
      $account = $member->getUser();
      $val = $account->get('field_external_id')->getValue();
      $val = reset($val);
      $memberExternalId = $val['value'];

      // Check if the participant is already in the group
      if (!is_null($memberExternalId)) {
        if (in_array($memberExternalId, $participantsExternalIds)) {
          $usersToSync[$memberExternalId] = $memberExternalId;
        }
        else {
          $groupMembersToRemove[$memberExternalId] = $memberExternalId;
        }
        unset($participantsToAdd[$memberExternalId]);
      }
    }
    $this->log('Participants sync in progress: ' . count($usersToSync) . ' user(s) will be updated, ' . count($participantsToAdd) . ' user(s) will be added, ' . count($groupMembersToRemove) . ' user(s) will be removed. Updated(' . implode('|', $usersToSync) . ') Added(' . implode('|', $participantsToAdd) . ') Removed(' . implode('|', $groupMembersToRemove) . ')');

    // Get Contacts from CiviCRM
    $contacts = $this->getContacts(array_keys($participantsToAdd + $usersToSync));
    foreach ($contacts as $contact) {

      // Check participant
      //@TODO check last update => update the user
      $userIds = \Drupal::entityQuery('user')
        ->condition('field_external_id', $contact->id, '=')
        ->execute();

      if (empty($userIds)) {

        // Create user
        try {
          $this->upsertUser($contact);
        } catch (\Exception $e) {
          \Drupal::logger('speakers_civicrm')->error('The user (civicrmId:%civi_id) could not be synced!', ['%civi_id' => $contact->id]);
          continue;
        }

        $userIds = \Drupal::entityQuery('user')
          ->condition('field_external_id', $contact->id, '=')
          ->execute();
        $userId = reset($userIds);
      }
      else {

        // Update user
        $userId = reset($userIds);
        try {
          $this->upsertUser($contact, $userId);
        } catch (\Exception $e) {
          \Drupal::logger('speakers_civicrm')->error('The user (civicrmId:%civi_id, uid:%uid) could not be synced! data:%data', ['%civi_id' => $contact->id, '%uid' => $userId, '%data' => json_encode($contact)]);
          continue;
        }
      }

      if (isset($participantsToAdd[$contact->id])) {

        // Add participant to group
        $roles = ['participant-speaker'];
        $values = ['group_roles' => $roles];
        $account = User::load($userId);
        $group->addMember($account, $values);
        $group->save();
      }
      else {

        // Update cancelled member to onboarded
        speakers_main_update_member_status('onboard', $group->id(), $userId);
      }
    }

    // Update member status removed on CiviCRM to cancelled
    foreach ($groupMembersToRemove as $externalId) {
      $userIds = \Drupal::entityQuery('user')
      ->condition('field_external_id', $externalId, '=')
      ->execute();

      $userId = reset($userIds);
      speakers_main_update_member_status('cancelled', $group->id(), $userId);
      //$account = User::load($userId);
      //$group->removeMember($account);
      //$group->save();
    }
  }

  /**
   * Update participants status on Civi
   *
   * @param int $eventId
   */
  public function updateEventParticipantsStatus($eventId = 0) {

    // Get participants from civi event
    $event = \Drupal::entityTypeManager()->getStorage('event')->load($eventId);
    $CiviEventId = $this->getCivicrmId($event);
    //$this->log("Participants status sync started on event $eventId (CiviId:$CiviEventId)");

    // Get Group members
    $members = $this->getMembers($event);

    // Check if status participant should be updated
    $result = [];
    foreach ($members as $member) {
      $account = $member->getUser();
      $field = $account->get('field_external_id')->getValue();
      $memberExternalId = $this->getValue($field);
      $field = $member->getGroupContent()->get('field_member_status')->getValue();
      $memberStatus = $this->getValue($field);

      if ($memberExternalId && $memberStatus) {
        switch ($memberStatus){
          case 'onboarded':
            $memberStatus = 'Attended';
            $this->updateStatus($CiviEventId, $memberExternalId, $memberStatus);
            break;
          case 'declined':
            $memberStatus = 'Rejected';
            $this->updateStatus($CiviEventId, $memberExternalId, $memberStatus);
            break;
          default:
            $memberStatus = 'Not updated';
            break;
        }
        $result[]= 'Id: ' . $memberExternalId . ', status: ' . $memberStatus;
      }
    }
    $this->log('Participants status sync done: CiviEventId: ' . $CiviEventId . ', result: ' . implode('|', $result));
  }

  /**
   * Check all participants in all published events
   */
  public function checkParticipantsUpdates() {
    $eventIds = \Drupal::entityQuery('event')
      ->condition('status', \Drupal\node\NodeInterface::PUBLISHED, '=')// @TODO add the property PUBLISHED to the event interface
      ->execute();
    foreach ($eventIds as $id => $event) {
      $this->checkEventParticipantsUpdates($id);
    }
  }

  public function addParticipantToQueue() {
    // Save in drupal var the participants list to update
  }

  public function processQueues() {
    $this->updateUser();
    $this->updateParticipant();
  }

  public function addUserToQueue() {
    // Save in drupal var the user list to update
  }

  /**
   * Create or update a user
   *
   * @param array $data
   *
   * @return int
   */
  public function upsertUser($data = [], $userId = 0) {
    //$language = \Drupal::languageManager()->getCurrentLanguage()->getId();

    if (empty($userId)) {

      // Create user
      $account = User::create();

      // Mandatory settings
      $account->setPassword('2Fhb&31!fGv-0' . rand());
      $account->enforceIsNew();

    }
    else {

      // Update user
      $account = User::load($userId);
    }

    // Mandatory settings
    $account->setEmail($data->email);
    $account->setUsername($data->email); //This username must be unique and accept only a-Z,0-9, - _ @ .

    // Optional settings
    $account->set("field_external_id", $data->id);
    $account->set("field_prefix", isset($data->prefix)?$data->prefix:'');
    $account->set("field_suffix", isset($data->suffix)?$data->suffix:'');
    $account->set("field_first_name", isset($data->first_name)?$data->first_name:'');
    $account->set("field_middle_name", isset($data->middle_name)?$data->middle_name:'');
    $account->set("field_last_name", isset($data->last_name)?$data->last_name:'');
    $account->set("field_phone", isset($data->phone)?$data->phone:'');
    $account->set("field_country", isset($data->country)?$data->country:'');
    $account->set("field_email_greetings", isset($data->email_greeting_display)?$data->email_greeting_display:'');
    $account->activate();

    return $account->save();
  }

  /**
   * Get Group members
   *
   * @param \Drupal\event\Entity\EventInterface $event
   *
   * @return array
   */
  private function getMembers(EventInterface $event) {
    $groupContents = GroupContent::loadByEntity($event);
    unset($event);
    if (!is_array($groupContents) || empty($groupContents)) {
      $this->log("The group was not found.");
      return;
    }
    $groupContent = reset($groupContents);
    unset($groupContents);
    $group = $groupContent->getGroup();
    unset($groupContent);

    return $group->getMembers();
  }

  /**
   * Search the CiviCRM Event Id
   *
   * @param \Drupal\event\Entity\EventInterface $event
   *
   * @return bool
   */
  private function getCivicrmId(EventInterface $event) {
    $CiviEventId = FALSE;
    $CiviEventIdFieldGetValue = $event->get('field_civicrm_event_id')
      ->getValue();
    if (is_array($CiviEventIdFieldGetValue)) {
      $CiviEventIdFieldValues = reset($CiviEventIdFieldGetValue);
      if (isset($CiviEventIdFieldValues['value'])) {
        $CiviEventId = $CiviEventIdFieldValues['value'];
      }
    }

    return $CiviEventId;
  }

  /**
   * Get Field value
   *
   * @param $field
   *
   * @return bool
   */
  private function getValue($field) {
    $value = FALSE;

    if (is_array($field)) {
      $field = reset($field);
      if (isset($field['value'])) {
        $value = $field['value'];
      }
    }

    return $value;
  }

  /**
   * convert CiviCRM Date To SpeakersDate
   *
   * @param string $civiDate
   *
   * @return string
   */
  private function convertCiviDateToSpeakersDate($civiDate = 'string') {
    $d = \DateTime::createFromFormat('Y-m-d H:i:s', $civiDate);

    return $d->format('Y-m-d');
  }

  /**
   * Log a notice
   *
   * @param $message
   */
  private function log($message) {
    \Drupal::logger('speakers_civicrm')->notice($message);
    //\Drupal::logger('speakers_civicrm')->error($message);
    //\Drupal::messenger()->addError($message);
    //kint($message);
  }
}
