<?php

namespace Drupal\multi_session\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Multi session entities.
 *
 * @ingroup multi_session
 */
interface MultiSessionInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Multi session name.
   *
   * @return string
   *   Name of the Multi session.
   */
  public function getName();

  /**
   * Sets the Multi session name.
   *
   * @param string $name
   *   The Multi session name.
   *
   * @return \Drupal\multi_session\Entity\MultiSessionInterface
   *   The called Multi session entity.
   */
  public function setName($name);

  /**
   * Gets the Multi session creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Multi session.
   */
  public function getCreatedTime();

  /**
   * Sets the Multi session creation timestamp.
   *
   * @param int $timestamp
   *   The Multi session creation timestamp.
   *
   * @return \Drupal\multi_session\Entity\MultiSessionInterface
   *   The called Multi session entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Multi session revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Multi session revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\multi_session\Entity\MultiSessionInterface
   *   The called Multi session entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Multi session revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Multi session revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\multi_session\Entity\MultiSessionInterface
   *   The called Multi session entity.
   */
  public function setRevisionUserId($uid);

}
