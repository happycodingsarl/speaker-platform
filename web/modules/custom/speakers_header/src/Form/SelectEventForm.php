<?php

namespace Drupal\speakers_header\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Implementing a ajax form.
 */
class SelectEventForm extends FormBase implements ContainerInjectionInterface {

  /**
   * Symfony\Component\DependencyInjection\ContainerAwareInterface definition.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerAwareInterface
   */
  protected $entityQuery;

  /**
   * The entity manager
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Active request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  public $request;

  /**
   * Active account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  public $account;

  /**
   * SelectEventForm constructor.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param \Symfony\Component\DependencyInjection\ContainerAwareInterface $entity_query
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   */
  public function __construct(Request $request, ContainerAwareInterface $entity_query, EntityTypeManagerInterface $entity_type_manager, AccountProxyInterface $account) {
    $this->request = $request;
    $this->entityQuery = $entity_query;
    $this->entityTypeManager = $entity_type_manager;
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('entity.query'),
      $container->get('entity_type.manager'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'select_event_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $eids = '') {
    $event_list = [];
    $selectedEvent = '';
    // Need to check current year?
    // ->condition(date('Y', ['field_start_and_end_dates']['end_value']), date('Y'), '=');

    if (!empty($eids)) {
      $events_storage = $this->entityTypeManager->getStorage('event');
      $events = $events_storage->loadMultiple($eids);
      foreach ($events as $key => $event) {
        $event_list['title'][$key] = $event->name->value . ' - ' .
          $event->field_city->value . ' / ' .
          $event->field_country->value;
        if (!$event->status->value) {
          $event_list['title'][$key] .= ' (' . t('Unpublished') . ')';
        }
      }

      $session = $this->request->getSession();
      $selectedEvent = $session->has('selectedEvent') ? $session->get('selectedEvent') : current($eids);
    }

    $form['select_event'] = [
      '#prefix' => '<div id="select-event">',
      '#suffix' => '</div>',
      '#type' => 'select',
      '#options' => $event_list['title'],
      '#default_value' => $selectedEvent,
      '#attributes' => array('onchange' => "form.submit('select_event_form')"),
      // This code if we need ajax actions.
      /*'#ajax' => [
        'callback' => [$this, 'ajaxCallback'],
        'wrapper' => 'select-event',
      ],*/
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#attributes' => array('style' => 'display: none;'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxCallback($form, FormStateInterface $form_state) {
    /* $session = \Drupal::request()->getSession();
    $session->set('selectedEvent', $form_state->getValue('select_event'));
    $session->save();
    return $form['select_event'];*/
  }

  /**
   * {@inheritdoc}
   * @TODO move to current group module service
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $session = $this->request->getSession();

    $eventId = $form_state->getValue('select_event');
    $groupId = speakers_main_get_group_id($eventId);
    $session->set('selectedEvent', $eventId);
    $session->set('selectedGroupId', $groupId);
    $session->save();
  }

}
