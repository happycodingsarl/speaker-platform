<?php

namespace Drupal\speakers_header\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Provides a 'SpeakersFormHeader' block.
 *
 * @Block(
 *  id = "speakers_form_header",
 *  admin_label = @Translation("Speakers form header"),
 * )
 */
class SpeakersFormHeader extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Symfony\Component\DependencyInjection\ContainerAwareInterface definition.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerAwareInterface
   */
  protected $entityQuery;

  /**
   * The entity manager
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The FormBuilder object.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Creates a SpeakersFormHeader instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   * @param \Symfony\Component\DependencyInjection\ContainerAwareInterface $entity_query
   *  Query to entity.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ContainerAwareInterface $entity_query, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityQuery = $entity_query;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.query'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    if (!$this->entityTypeManager->hasDefinition('event')) {
      return $build;
    }
    $events = speaker_header_get_events_list();

    // Check if we have more than 1 Event.
    if (is_array($events) && count($events) > 1) {
      $build['event_form'] = [
        '#type' => 'container',
        '#prefix' => '<div id="event-form">',
        '#suffix' => '</div>',
      ];
      $build['event_form']['form'] = \Drupal::formBuilder()
        ->getForm('\Drupal\speakers_header\Form\SelectEventForm', $events);
    }
    return $build;
  }

}
