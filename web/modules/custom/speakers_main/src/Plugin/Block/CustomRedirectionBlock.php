<?php


namespace Drupal\speakers_main\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Block displaying links for emailing.
 *
 * @Block(
 *   id = "custom_redirection_block",
 *   admin_label = @Translation("Custom Redirection"),
 *   category = @Translation("Speakers Platform"),
 * )
 */
class CustomRedirectionBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Constructs a NodeEmbedBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityDisplayRepositoryInterface $entity_display_repository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    global $base_url;

    $routes = [
      'To the user dashboard: ' => 'speakers_main.my_dashboard',
      'To the user profile: ' => 'speakers_main.my_profile'
    ];
    $markup = '';

    foreach ($routes as $label => $route) {
      $url =  Url::fromRoute($route)->toString();
      $markup .= $label . $base_url . $url . '<br>';
    }

    return [
      '#markup' => $markup,
    ];
  }
}
