/**
 * @file
 *
 * Transforms data from the backend into something more
 * beautiful and readable on the frontend.
 */

/**
 * Session
 *
 * @param data
 * @returns {{resourceId: (*|string), speaker: string, start: (*|string), topic: (*|string), end: (*|string), language: (*|string), id: *, title: (*), category: (*|string), cerp: *, status: (*|string)}}
 */
export const session = data => ({
  id: data.entityId.id,
  title: data.entityId.attributes.field_title != null ? data.entityId.attributes.field_title : data.entityId.title,
  resourceId:  data.entityId.relationships.field_room.data != null ? data.entityId.relationships.field_room.data.id : '',  //'fb6d0741-19a1-4e68-8269-4c054d5da708'
  start: data.entityId.fieldScheduledDate ? data.entityId.fieldScheduledDate.value : '', //'2019-09-08T14:00:00',
  end: data.entityId.fieldScheduledDate ? data.entityId.fieldScheduledDate.end_value : '',//'2019-09-08T18:00:00',
  language: data.entityId.attributes.field_language != null ? data.entityId.attributes.field_language : 'English',
  speaker: data.entityId.fieldParticipant.length > 0 ? data.entityId.fieldParticipant[0].fieldFirstName + ' ' + data.entityId.fieldParticipant[0].fieldLastName : '',
  category: data.entityId.fieldSessionCategory.name != null ? data.entityId.fieldSessionCategory.name : '',
  topic: data.entityId.fieldSessionTopic !== undefined ? data.entityId.fieldSessionTopic.attributes.name : '',
  status: data.entityId.attributes.field_session_status ? data.entityId.attributes.field_session_status : '',
  cerp: data.entityId.attributes.field_session_cerp
})

/**
 * multiSession
 *
 * @param data
 * @returns {{resourceId: string, start: (*|string), end: (*|string), id: *, title: (*|string)}}
 */
export const multiSession = data => ({
  id: data.id,
  title: data.name != null ? data.name : 'No title defined!',
  resourceId:  data.fieldSession.length > 0 ? data.fieldSession[0].relationships.field_room.data.id + '-multiple': '',
  start: data.fieldSession.length > 0 && data.fieldSession[0].fieldScheduledDate != null ? data.fieldSession[0].fieldScheduledDate.value : '',
  end: data.fieldSession.length > 0 && data.fieldSession[data.fieldSession.length-1].fieldScheduledDate != null ? data.fieldSession[data.fieldSession.length-1].fieldScheduledDate.end_value : '',
  eid: data.drupalInternalId,
  sessions: data.fieldSession.length > 0 ? data.fieldSession : []
})

/**
 * breakTimeSlot
 *
 * @param data
 * @returns {{resourceId: string, start: *, end: *, id: string, title: (*|string)}}
 */
export const breakTimeSlot = data => ({
  id: data.id + '-break',
  title: data.name != null ? data.name : 'Break',
  resourceId:  data.relationships.field_room.data != null ? data.relationships.field_room.data.id : '',//'91d7bd9e-e19c-4b0d-b3b1-9bde866707d1'
  start: data.fieldDate.value,
  end: data.fieldDate.end_value
})

/**
 * Event
 *
 * @param data
 * @returns {{eventId: *, groupId: (*|string), start: (*|string), end: (*|string)}}
 */
export const event = data => ({
  eventId: data.eventId,
  groupId: data.groupId ? data.groupId : '',
  start: data.startDate ? data.startDate : '',
  end: data.endDate ? data.endDate : ''
});

/**
 * Room
 * see https://fullcalendar.io/docs/resource-parsing
 *
 * @param data
 * @returns {{id: *, title: *}}
 */
export const room = data => ({
  id: data.entityId.id + '-multiple',
  title: data.entityId.name,
  code: parseInt(data.entityId.fieldCode),
  children: [
    {
      id: data.entityId.id,
      title: 'Presentations'
    },
  ]
});
