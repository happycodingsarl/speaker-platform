<?php

namespace Drupal\break_time_slot;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\break_time_slot\Entity\BreakTimeSlotInterface;

/**
 * Defines the storage handler class for Break time slot entities.
 *
 * This extends the base storage class, adding required special handling for
 * Break time slot entities.
 *
 * @ingroup break_time_slot
 */
interface BreakTimeSlotStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Break time slot revision IDs for a specific Break time slot.
   *
   * @param \Drupal\break_time_slot\Entity\BreakTimeSlotInterface $entity
   *   The Break time slot entity.
   *
   * @return int[]
   *   Break time slot revision IDs (in ascending order).
   */
  public function revisionIds(BreakTimeSlotInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Break time slot author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Break time slot revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

}
