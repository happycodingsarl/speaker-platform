<?php

/**
 * @file
 *
 */

namespace Drupal\speakers_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\migrate\Plugin\migrate\destination\Entity;
use Drupal\node\Entity\Node;
use Drupal\image\Entity\ImageStyle;
use Drupal\locale\SourceString;
use Drupal\Core\StringTranslation\Translator;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller for api routes.
 */
class SpeakersAPIController extends ControllerBase {

  /**
   * Get event info
   * 
   * Callback for `speakers-api/v1/get.json` API method.
   */
  public function getEvent(Request $request) {
    $eventId = speakers_main_get_event_id();
    $event = \Drupal::entityTypeManager()
      ->getStorage('event')
      ->load($eventId);
    $data = [];
    if (!is_null($event)) {
      $data = [
        'eventId' => $eventId,
        'groupId' => speakers_main_get_group_id($eventId),
        'title' => $event->get('name')->getValue(),
        'startDate' => $event->field_start_and_end_dates->value,
        'endDate' => $event->field_start_and_end_dates->end_value
      ];
    }

    $response['data'] = $data;
    $response['method'] = 'GET';

    return new JsonResponse($response);
  }
}
