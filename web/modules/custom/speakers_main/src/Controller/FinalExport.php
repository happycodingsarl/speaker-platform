<?php

namespace Drupal\speakers_main\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides data for Final Program.
 */
class FinalExport extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(ContainerInterface $container) {
    $this->entityTypeManager = $container->get('entity_type.manager');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container);
  }

  /**
   * Returns a page.
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function FinalExportView() {
    $firstLevel = \Drupal::service('speakers.final_export')->getFirstLevel();
    $timeCodes = \Drupal::service('speakers.final_export')->getCodeTime();
    $daysCode = \Drupal::service('speakers.final_export')->getDaysCode();
    list($rooms, $roomsSorted) = \Drupal::service('speakers.final_export')->getCodeRooms();
    $previousLabel = '';
    $previousDay = '';
    $scheduledSpeakers = [];
    $block_render = [];

    //TODO: implement a cache logic
    foreach ($firstLevel as $firstLevelElement) {
      if ($firstLevelElement->type === 'session') {
        $entity_type = 'node';
        $view_mode = 'final_program';

        $node = \Drupal::entityTypeManager()
          ->getStorage($entity_type)
          ->load($firstLevelElement->id);

        // Calculate code & Save it for each speaker
        $scheduledSpeakers[$node->field_participant->target_id][] = $code = \Drupal::service('speakers.final_export')->calculateCode($firstLevelElement->id, $firstLevelElement->startDate, $firstLevelElement->endDate, $node->field_room->target_id);

        // Period title & Day display
        $this->displayPeriodDay($node->field_scheduled_date->value,$previousDay,$previousLabel,$block_render);

        // Get topics
        foreach ($node->field_keywords as $field){
          $keywordsList[$field->target_id][] = $code;
        }

        // TODO: make a service to generate speaker name
        $speakerName = implode(' ', [
          $node->get('field_participant')->entity->get('field_first_name')->value,
          $node->get('field_participant')->entity->get('field_last_name')->value,
        ]);
        $country = '(' . $node->get('field_participant')->entity->get('field_country')->value . ')';

        $startDate = new DrupalDateTime($node->field_scheduled_date->value, DateTimeItemInterface::STORAGE_TIMEZONE);
        $startDateFormatted = $startDate->format('H:i', ['timezone' => 'Europe/Zurich']);
        $endDate = new DrupalDateTime($node->field_scheduled_date->end_value, DateTimeItemInterface::STORAGE_TIMEZONE);
        $endDateFormatted = $endDate->format('H:i', ['timezone' => 'Europe/Zurich']);

        $languageCode = $node->get('field_language')->value;
        $language = GetLanguageFullName($languageCode);

        $chair = $node->get('field_session_chair')->value;

        $block_render[] = [
          '#theme' => 'final_program_session_unique',
          '#code_scheduler' => $code,
          '#title' => $node->get('field_title')->value,
          '#subtitle' => $node->get('field_subtitle')->value,
          '#time' => $startDateFormatted . ' - ' . $endDateFormatted,
          '#room' => $node->get('field_room')->entity->label() . ' (' . $node->get('field_room')->entity->get('field_code')->value . ')',
          '#language' => $language,
          '#chairs' => [$chair],
          '#cerp' => $node->get('field_session_cerp')->value,
          '#speakers' => [
            $speakerName . ' ' . $country
          ],
          '#rendered_session' => [
            '#markup' => $this->entityView($node, $entity_type, $view_mode)
          ],
        ];

      }
      else {

        // Multiple
        $entity_type = 'multi_session';
        $view_mode = 'default';

        $entity = \Drupal::entityTypeManager()
          ->getStorage($entity_type)
          ->load($firstLevelElement->id);

        // Load sessions related
        $i = $multiSessionCERP = $participantId = 0;
        $time = $language = $room = 'Not found!';
        $start = $end = $speakers = $keywords = [];

        foreach ($entity->field_session->referencedEntities() as $session) {
          $multiSessionCERP += $session->field_session_cerp->value;

          // Get sessions dates
          if (!is_null($session->field_scheduled_date)) {
            $startDate = new DrupalDateTime($session->field_scheduled_date->value, DateTimeItemInterface::STORAGE_TIMEZONE);
            $startDateFormatted = $startDate->format('H:i', ['timezone' => 'Europe/Zurich']);
            $startDateTimestamp = $startDate->getTimestamp();
            $start[$startDateTimestamp]['formatted'] = $startDateFormatted;
            $start[$startDateTimestamp]['default'] = $session->field_scheduled_date->value;
            $endDate = new DrupalDateTime($session->field_scheduled_date->end_value, DateTimeItemInterface::STORAGE_TIMEZONE);
            $endDateFormatted = $endDate->format('H:i', ['timezone' => 'Europe/Zurich']);
            $endDateTimestamp = $endDate->getTimestamp();
            $end[$endDateTimestamp]['formatted'] = $endDateFormatted;
            $end[$endDateTimestamp]['default'] = $session->field_scheduled_date->end_value;
          }

          if ($session->field_participant != null) {
            $user = \Drupal::entityTypeManager()
              ->getStorage('user')
              ->load($session->field_participant->target_id);

            $speakers[] =
              $user->field_first_name->value
              . ' ' . $user->field_last_name->value
              . ' (' . $user->field_country->value . ')'
            ;
          }

          // Get session keywords
          foreach ($session->get('field_keywords') as $field){
            $keywords[] = $field->target_id;
          }

          if ($i == 0) {
            $roomId = !is_null($session->field_room) ? $session->field_room->target_id: 0;
            $roomName = !empty($rooms[$roomId]) ? $rooms[$roomId]['label'] : '';
            $roomCode = !empty($rooms[$roomId]) ? $rooms[$roomId]['code'] : '';
            $participantId = $session->field_participant->target_id;
            $languageCode = $session->field_language->value;
            $language = GetLanguageFullName($languageCode);
          }
          $i++;
        }

        // Calculate code & Save it for each speaker
        $scheduledSpeakers[$participantId][] = $code = \Drupal::service('speakers.final_export')->calculateCode($firstLevelElement->id, $start[min(array_keys($start))]['default'], $end[max(array_keys($end))]['default'], $roomId);

        // Period title & Day display
        $this->displayPeriodDay($start[min(array_keys($start))]['default'],$previousDay,$previousLabel,$block_render);

        // Get codes by keyword
        foreach ($keywords as $termId){
          $keywordsList[$termId][] = $code;
        }

        if (!empty($start)) {
          $time = $start[min(array_keys($start))]['formatted'] . ' - ' . $end[max(array_keys($end))]['formatted'];
        }

        $chairs = [];
        foreach ($entity->field_chair as $chair) {
          $chairs[] = $chair->value;
        }

        $block_render[] = [
          '#theme' => 'final_program_multisession',
          '#code_scheduler' => $code,
          '#title' => $entity->label(),
          '#time' => $time,
          '#room' => $roomName . ' (' . $roomCode . ')',
          '#language' => $language,
          '#chairs' => $chairs,
          '#cerp' => $multiSessionCERP,
          '#speakers' => $speakers,
          '#rendered_session' => [
            '#markup' => $this->entityView($entity, $entity_type, $view_mode)
            ],
        ];

      }
    }

    // Save the speakers codes to retrieve it in other service and pages either if it's empty
    \Drupal::service('speakers.final_export')->speakerCodes($scheduledSpeakers);

    // Save the codes by keyword
    \Drupal::service('speakers.final_export')->codesByKeyword($keywordsList);

    // Store state to not rebuild it every access time
    // TODO: create a function triggered by button to delete this state from cache or use the cache system even better
    // For the moment clear cache with commande: "drupal state:delete speakers.final_data"
    /*\Drupal::state()->set('speakers.final_data', [
      'daysCode' => $daysCode,
      'timeCode' => $timeCodes,
      'roomsCode' => $rooms,
      'sessions' => $firstLevelMarkup
    ]);*/

    $code_markup = [
      '#markup' =>
        $this->getCodeMarkup($daysCode, 'days', 'Day Codes')
        . $this->getCodeTimeMarkup($timeCodes)
        . $this->getCodeMarkup($roomsSorted, 'rooms', 'Room codes')
      ,
    ];

    $final_render = [
      '#type' => 'html',
      '#theme' => 'final_program_page',
      '#codes' => $code_markup,
      '#sessions' => $block_render,
      // '#cache' => ['max-age' => 0],
    ];

    return $final_render;
  }

  /**
   * Keywords list page
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function keywordsList() {
    $blocks = [];
    $keywordsList = \Drupal::service('speakers.final_export')->codesByKeyword();

    // Order by term label
    $termIds = \Drupal::entityQuery('taxonomy_term')
      ->condition('tid', array_keys($keywordsList), 'IN')
      ->sort('name')
      ->execute();

    // Generate keyword items
    $terms = $this->entityTypeManager->getStorage('taxonomy_term')
      ->loadMultiple($termIds);

    foreach ($terms as $termId => $term) {
      $codes =$keywordsList[$termId];
      $blocks[] = [
        '#theme' => 'final_program_keyword',
        '#name' => $term->getName(),
        '#codes' => implode(', ', $codes)
      ];
    }

    // No Keyword
    if (empty($blocks)) {
      $blocks[] = [
        '#theme' => 'final_program_keyword',
        '#name' => 'Please visit the Final Program first in order to generate the Keyword list.',
        '#codes' => ''
      ];
    }

    // Render
    return [
      '#theme' => 'final-program-keywords-list-page',
      '#title' => 'Keywords list',
      '#class' => 'keywords-list',
      '#keywords' => $blocks,
      '#cache' => ['max-age' => 0]
    ];
  }

  public function SpeakerListView() {

    // Get Speaker codes
    $speakerCodes = \Drupal::service('speakers.final_export')->speakerCodes();

    $firstLevel = \Drupal::service('speakers.final_export')->participants();
    $firstLevelMarkup = $lastRecord = '';

    $firstLevelMarkup .= '<div class="final_program_list_speakers">';

    foreach ($firstLevel as $entity) {
      $code = 'Not found!';
      if (isset($speakerCodes[$entity->uid])) {
        $code = implode(', ', $speakerCodes[$entity->uid]);
      }

      $firstLevelMarkup .= '<div class="speaker">';
      //$firstLevelMarkup .= '<div class="field__user-id">User id: ' . $entity->uid . ' (to remove)</div>';
      $firstLevelMarkup .= '<div class="field__session_code">' . $code . '</div>';
      $firstLevelMarkup .= '<div class="field__name">' . $entity->ulname . ', ' . $entity->ufname . '</div>';
      $firstLevelMarkup .= '<div class="field__page">page: </div>';
      $firstLevelMarkup .= '<div class="field__bio">' . $entity->ubio . '</div>';
      $firstLevelMarkup .= '</div>';
    }

    $firstLevelMarkup .= '</div>';

    return array(
      '#markup' => $firstLevelMarkup,
    );
  }

  /**
   * Generate codeTime table
   *
   * @param array $timeCodes
   * @return string
   */
  private function getCodeTimeMarkup($timeCodes = []) {
    $codeTimeMarkup = '<div class="code times" style=""><h2>Time codes</h2><table>';

    foreach ($timeCodes as $codeTimeId => $codeTime) {
      $codeTimeMarkup .= '<tr><td>' . $codeTime['code'] . '</td><td>' . $codeTime['startDate'] . ' - ' . $codeTime['endDate'] . '</td></tr>';
    }

    $codeTimeMarkup .= '</table></div>';

    //$header = array('Code Times', '');
    //return theme('table', array('header' => $header, 'rows' => $codeTimes));

    return $codeTimeMarkup;
  }

  /**
   * Generate codeDay table
   * @TODO twig
   *
   * @param array $daysCode
   * @param string $class
   * @return string
   */
  private function getCodeMarkup($daysCode = [], $class = '', $label = '') {
    $codeTimeMarkup = "<div class='code $class' style='width:30%;float:left;'><h2>$label</h2><table>";

    foreach ($daysCode as $day) {
      $codeTimeMarkup .= '<tr>';
      foreach ($day as $value) {
        $codeTimeMarkup .= '<td>' . $value . '</td>';
      }
      $codeTimeMarkup .= '</tr>';
    }

    $codeTimeMarkup .= '</table></div>';// . '</PRE>' . print_r($result) . '</PRE>';

    //$header = array('Code Times', '');
    //return theme('table', array('header' => $header, 'rows' => $codeTimes));

    return $codeTimeMarkup;
  }

  /**
   * View of an entity
   *
   * @param $entity
   * @param $entity_type
   * @param $view_mode
   * @return mixed|null
   */
  private function entityView($entity, $entity_type, $view_mode) {
    $nodeView = \Drupal::entityTypeManager()
      ->getViewBuilder($entity_type)
      ->view($entity, $view_mode);

    return render($nodeView);
  }

  /**
   * Display Period title (Morning session) & Day
   *
   * @param $scheduledStartDate
   * @param $previousDay
   * @param $previousLabel
   * @param $block_render
   */
  private function displayPeriodDay($scheduledStartDate, &$previousDay, &$previousLabel, &$block_render) {
    $periods = \Drupal::service('speakers.final_export')->periods();
    $sessionStartDate = new DrupalDateTime($scheduledStartDate, DateTimeItemInterface::STORAGE_TIMEZONE);
    $sessionStartDateFormatted = $sessionStartDate->format('Hi', ['timezone' => 'Europe/Zurich']);
    $currentDay = $sessionStartDate->format('l F j Y', ['timezone' => 'Europe/Zurich']);

    // Add day date to wrap sessions list
    if (!$previousDay || (isset($previousDay) && $previousDay != $currentDay)) {
      $block_render[] = [
        '#markup' => '<br /><br /><h3 class="field-day-date">' . $currentDay . '</h3><div>&nbsp;</div>',
      ];
      $previousDay = $currentDay;
    }

    foreach ($periods as $date => $label) {
      $periodStartDate = new DrupalDateTime($date, DateTimeItemInterface::STORAGE_TIMEZONE);
      $periodStartDateFormatted = $periodStartDate->format('Hi', ['timezone' => 'Europe/Zurich']);

      if ($sessionStartDateFormatted >= $periodStartDateFormatted) {
        if ($label . $currentDay != $previousLabel) {
          $block_render[] = [
            '#markup' => '<div class="field-period label field__item">' . $label . '</div><br /><br />',
          ];

          $previousLabel = $label . $currentDay;
        }
        break;
      }
    }
  }
}
