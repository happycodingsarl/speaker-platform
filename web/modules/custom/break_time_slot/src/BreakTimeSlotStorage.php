<?php

namespace Drupal\break_time_slot;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\break_time_slot\Entity\BreakTimeSlotInterface;

/**
 * Defines the storage handler class for Break time slot entities.
 *
 * This extends the base storage class, adding required special handling for
 * Break time slot entities.
 *
 * @ingroup break_time_slot
 */
class BreakTimeSlotStorage extends SqlContentEntityStorage implements BreakTimeSlotStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(BreakTimeSlotInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {break_time_slot_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {break_time_slot_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

}
