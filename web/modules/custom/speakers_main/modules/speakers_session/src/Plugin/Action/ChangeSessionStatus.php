<?php

namespace Drupal\speakers_session\Plugin\Action;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsPreconfigurationInterface;

/**
 * Action: Change Session Status.
 * https://www.drupal.org/docs/8/modules/views-bulk-operations-vbo/creating-a-new-action
 *
 * @Action(
 *   id = "speakers_session_change_session_status",
 *   label = @Translation("Change Session Status"),
 *   type = "",
 *   confirm = FALSE,
 *   requirements = {
 *     "_permission" = "administer event entities",
 *     "_custom_access" = FALSE,
 *   },
 *   pass_view = TRUE
 * )
 */
class ChangeSessionStatus extends ViewsBulkOperationsActionBase {

  use StringTranslationTrait;
  use MessengerTrait;

  /**
   * Configuration form builder.
   *
   * If this method has implementation, the action is
   * considered to be configurable.
   *
   * @param array $form
   *   Form array.
   * @param FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The configuration form.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['speakers_session_config_status'] = [
      '#type' => 'select',
      '#title' => $this->t('Choose the status'),
      '#default_value' => 'accepted',
      '#options' => ['accepted' => 'Accepted', 'correctionNeeded' => 'Correction Needed', 'declined' => 'Declined'],
      '#description' => 'The chosen status will be set to all selected sessions',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    // Do some processing..

    // Don't return anything for a default completion message, otherwise return translatable markup.
    return $this->t('Some result');
  }

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $entities) {
    $ok = $ko = FALSE;

    // Change Status only on this view
    if ($this->context['view_id'] == 'session_list') {

      // Update statuses
      $newStatus = $this->configuration['speakers_session_config_status'];
      foreach ($entities as $delta => $entity) {
        if (($oldStatus = $entity->field_session_status->value) && (in_array($oldStatus, [
            'accepted',
            'correctionNeeded',
            'declined',
            'proposed'
          ]))) {

          // Save new Status
          $entity->set('field_session_status', $newStatus);
          $entity->save();

          if (!$ok) {
            $ok = $this->t('The status has been changed to "@n" on these sessions: ', [
              '@n' => $newStatus
            ]);
          }
          $ok .= $this->t('@t(@n), ', [
            '@t' => $entity->title->value,
            '@n' => $entity->nid->value
          ]);

        }
        else {
          if (!$ko) {
            $ko = $this->t('The status cannot be changed on these sessions: ');
          }
          $ko .= $this->t('@t(@n), ', [
            '@t' => $entity->title->value,
            '@n' => $entity->nid->value
          ]);
        }
      }
    }
    else {
      $ko = 'Nothing done.';
    }

    // Display messages
    if ($ok) {
      $this->messenger()->addStatus($ok);
    }
    if ($ko) {
      $this->messenger()->addWarning($ko);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($object->getEntityType() === 'node') {
      $access = $object->access('update', $account, TRUE)
        ->andIf($object->status->access('edit', $account, TRUE));
      return $return_as_object ? $access : $access->isAllowed();
    }

    // Other entity types may have different
    // access methods and properties.
    return TRUE;
  }
}
