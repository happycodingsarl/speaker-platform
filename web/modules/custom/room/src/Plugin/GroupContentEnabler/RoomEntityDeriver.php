<?php
/**
 * Created by Happy coder ;)
 */

namespace Drupal\room\Plugin\GroupContentEnabler;

use Drupal\Component\Plugin\Derivative\DeriverBase;

class RoomEntityDeriver extends DeriverBase {

  /**
   * {@inheritdoc}.
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives['default'] = [
        'entity_bundle' => 'default',
        'label' => t('Group room (default)'),
        'description' => t('Adds a room to groups.'),
      ] + $base_plugin_definition;
    return $this->derivatives;
  }
}
