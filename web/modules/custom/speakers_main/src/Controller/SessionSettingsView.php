<?php
/**
 * Created by Happy Coder
 */

namespace Drupal\speakers_main\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\Renderer;
use Drupal\group\Entity\GroupContent;

/**
 * Provides context filter by Group in view.
 */
class SessionSettingsView extends ControllerBase {

  /**
   * @var Renderer
   */
  protected $renderer;

  public function __construct(Renderer $renderer) {
    $this->renderer = $renderer;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * Get the rendered view of session settings list
   *
   * @param string $user_id
   *   The participant_id to filter by, or NULL to log out.
   * @return array|null
   */
  public function sessionSettingsList($user_id) {
    $content = [];
    $groupId = speakers_main_get_group_id();

    // Get renderable array for view.
    $view = views_embed_view('session_settings', 'session_settings_list', $user_id, $groupId);

    // Render view. @TODO remove
    $content['#markup'] = $this->renderer->render($view);

    return $view;
  }

}
