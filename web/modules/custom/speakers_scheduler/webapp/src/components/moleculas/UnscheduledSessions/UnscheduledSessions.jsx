import React from 'react'
import Filter from '../../atoms/Filter/Filter'

// Import standard style from this component
import './style.scss'
import * as transforms from "../../../services/transforms";

export default class UnscheduledSessions extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidUpdate(prevProps, prevState, snapshot)  {
    if (prevProps.events !== this.props.events) {

      console.log(this.state.selectedOption,'this.state.selectedOption')
      console.log(this.state.categories,'this.state.categories')

      // Update events
      this.setState({
        events: this.props.events,
        speakers: this.getUnique(this.props.events.map(event => this.speaker(event)), 'value'),
        titles: this.getUnique(this.props.events.map(event => this.title(event)).sort((a, b) => a.label.localeCompare(b.label)), 'value'),
        categories: this.getUnique(this.props.events.map(event => this.category(event)).sort((a, b) => a.label.localeCompare(b.label)), 'value'),
        topics: this.getUnique(this.props.events.map(event => this.topic(event)).sort((a, b) => a.label.localeCompare(b.label)), 'value')
      }, () => {

        // Update filters
        this.setState({
          selectedOption: {
            speaker: this.keepFilterIfExist('speaker', 'speakers'),
            title: this.keepFilterIfExist('title', 'titles'),
            category: this.keepFilterIfExist('category', 'categories'),
            topic: this.keepFilterIfExist('topic', 'topics')
          },
        })
      })
      //console.log(this.state, 'state componentDidUpdate')
    }
  }

  state = {
    events: [],
    speakers: [{value: 'Speaker name', label: 'Speaker name'}],
    titles: [{value: 'Title', label: 'Title'}],
    categories: [{value: 'Title', label: 'Title'}],
    topics: [{value: 'Topics', label: 'Topics'}],
    selectedOption: {
      speaker: null,
      title: null,
      category: null,
      topic: null
    },
    selected: ''
  }

  /**
   * Check if filter is always in the filter list
   *
   * @param filter
   * @param filterList
   * @returns {null}
   */
  keepFilterIfExist = (filter = '', filterList = {}) => {
    let filterValue = null

    if (this.state.selectedOption[filter] != null) {
      filterValue = this.state[filterList].find(obj => {
        return obj.value === this.state.selectedOption[filter].value
      })
    }

    if (typeof (filterValue) == 'undefined') {
      filterValue = null
    }
    //console.log(filterValue, filter, 'filterValue componentDidUpdate')

    return filterValue
  }

  /**
   * Format for speakers filter
   *
   * @param event
   * @returns {{label: *, value: *}}
   */
  speaker = event => ({
    value: event.speaker,
    label: event.speaker
  })

  /**
   * Format for titles filter
   *
   * @param event
   * @returns {{label: *, value: *}}
   */
  title = event => ({
    value: event.title,
    label: event.title
  })

  /**
   * Format for categories filter
   *
   * @param event
   * @returns {{label: *, value: *}}
   */
  category = event => ({
    value: event.category,
    label: event.category
  })

  /**
   * Format for topics filter
   *
   * @param event
   * @returns {{label: *, value: *}}
   */
  topic = event => ({
    value: event.topic,
    label: event.topic
  })

  /**
   * Remove duplicate objects from an array
   *
   * see https://reactgo.com/removeduplicateobjects/
   *
   * @param arr
   * @param comp
   * @returns {*}
   */
  getUnique = (arr, comp) => {
    const unique = arr
      .map(e => e[comp])

      // store the keys of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)

      // eliminate the dead keys & store unique objects
      .filter(e => arr[e]).map(e => arr[e]);

    return unique;
  }

  /**
   * handle Select
   *
   * @param selected
   * @param prop
   */
  handleSelect = (selected = '', prop = '') => {
    let newEvents
    if (selected != null) {
      newEvents = this.props.events.filter(function (event) {
        return event[prop] === selected.value
      })
    }
    else{
      newEvents = this.props.events
    }

    this.setState({
      'events': newEvents,
      'selectedOption':
        {
          speaker: prop === 'speaker' ? selected : null,
          title: prop === 'title' ? selected : null,
          category: prop === 'category' ? selected : null,
          topic: prop === 'topic' ? selected : null
        }
    })
    //console.log(this.state, 'state handleSelect')
  }

  render() {
    const {speakers, titles, categories, topics, events} = this.state
    return (
      <div id="external-events" className="left-column">
        <h2>Presentation list</h2>
        <Filter
          name={'name'}
          options={speakers}
          selectedOption={this.state.selectedOption.speaker}
          onSelect={
            selected => this.handleSelect(selected, 'speaker')
          }
        />
        <Filter
          name={'title'}
          options={titles}
          selectedOption={this.state.selectedOption.title}
          onSelect={
            selected => this.handleSelect(selected, 'title')
          }
        />
        <Filter
          name={'category'}
          options={categories}
          selectedOption={this.state.selectedOption.category}
          onSelect={
            selected => this.handleSelect(selected, 'category')
          }
        />
        <Filter
          name={'topic'}
          options={topics}
          selectedOption={this.state.selectedOption.topic}
          onSelect={
            selected => this.handleSelect(selected, 'topic')
          }
        />
        {events.map((event, index) => {
          return (
            <div
              className="fc-event"
              title={event.title}
              id={event.id}
              key={event.id}
              speaker={event.speaker}
              category={event.category}
              topic={event.topic}
              language={event.language}
            >
              <span className="event--speaker">{event.speaker}</span>
              <span className="event--title">{event.title}</span>
              <span className="label label--category">{event.category}</span>
              <span className="label label--topic">{event.topic}</span>
            </div>
          )
        })}
      </div>
    )
  }
}

