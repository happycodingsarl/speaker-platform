<?php

/**
 * @file
 * Contains break_time_slot.page.inc.
 *
 * Page callback for Break time slot entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Break time slot templates.
 *
 * Default template: break_time_slot.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_break_time_slot(array &$variables) {
  // Fetch BreakTimeSlot Entity Object.
  $break_time_slot = $variables['elements']['#break_time_slot'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
