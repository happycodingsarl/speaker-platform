<?php

namespace Drupal\break_time_slot\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;
use Drupal\break_time_slot\Entity\BreakTimeSlotInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BreakTimeSlotController.
 *
 *  Returns responses for Break time slot routes.
 */
class BreakTimeSlotController extends ControllerBase implements ContainerInjectionInterface {


  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Constructs a new BreakTimeSlotController.
   *
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   */
  public function __construct(DateFormatter $date_formatter, Renderer $renderer) {
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * Displays a Break time slot revision.
   *
   * @param int $break_time_slot_revision
   *   The Break time slot revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($break_time_slot_revision) {
    $break_time_slot = $this->entityTypeManager()->getStorage('break_time_slot')
      ->loadRevision($break_time_slot_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('break_time_slot');

    return $view_builder->view($break_time_slot);
  }

  /**
   * Page title callback for a Break time slot revision.
   *
   * @param int $break_time_slot_revision
   *   The Break time slot revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($break_time_slot_revision) {
    $break_time_slot = $this->entityTypeManager()->getStorage('break_time_slot')
      ->loadRevision($break_time_slot_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $break_time_slot->label(),
      '%date' => $this->dateFormatter->format($break_time_slot->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Break time slot.
   *
   * @param \Drupal\break_time_slot\Entity\BreakTimeSlotInterface $break_time_slot
   *   A Break time slot object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(BreakTimeSlotInterface $break_time_slot) {
    $account = $this->currentUser();
    $break_time_slot_storage = $this->entityTypeManager()->getStorage('break_time_slot');

    $build['#title'] = $this->t('Revisions for %title', ['%title' => $break_time_slot->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all break time slot revisions") || $account->hasPermission('administer break time slot entities')));
    $delete_permission = (($account->hasPermission("delete all break time slot revisions") || $account->hasPermission('administer break time slot entities')));

    $rows = [];

    $vids = $break_time_slot_storage->revisionIds($break_time_slot);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\break_time_slot\BreakTimeSlotInterface $revision */
      $revision = $break_time_slot_storage->loadRevision($vid);
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $break_time_slot->getRevisionId()) {
          $link = $this->l($date, new Url('entity.break_time_slot.revision', [
            'break_time_slot' => $break_time_slot->id(),
            'break_time_slot_revision' => $vid,
          ]));
        }
        else {
          $link = $break_time_slot->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => Url::fromRoute('entity.break_time_slot.revision_revert', [
                'break_time_slot' => $break_time_slot->id(),
                'break_time_slot_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.break_time_slot.revision_delete', [
                'break_time_slot' => $break_time_slot->id(),
                'break_time_slot_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
    }

    $build['break_time_slot_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
