<?php
/**
 * Created by Happy Coder
 */

namespace Drupal\speakers_main\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\speakers_main\GroupService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\Renderer;
use Drupal\group\Entity\GroupContent;

class ContextualView extends ControllerBase {

  /**
   * @var Renderer
   */
  protected $renderer;

  public function __construct(Renderer $renderer) {
    $this->renderer = $renderer;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * Get the rendered view of participants list
   *
   * @return array|null
   */
  public function participantsList() {

    // Get group id
    $groupId = \Drupal::service('speakers.group')->getEventGroupId();

    // get renderable array for view
    $view = views_embed_view('participants_list', 'page_1', $groupId);

    return $view;
  }

  /**
   * Get the rendered view of session list
   *
   * @return array|null
   */
  public function sessionsList() {

    // Get group id
    $groupId = \Drupal::service('speakers.group')->getEventGroupId();

    // get renderable array for view
    $view = views_embed_view('session_list', 'page_1', $groupId);

    return $view;
  }

  /**
   * Get the rendered view of participants list
   *
   * @return array|null
   */
  public function speakerCOI() {

    // Get group id
    $groupId = \Drupal::service('speakers.group')->getEventGroupId();

    // get renderable array for view
    $view = views_embed_view('final_program_coi', 'block_1', $groupId);

    return $view;
  }

}
