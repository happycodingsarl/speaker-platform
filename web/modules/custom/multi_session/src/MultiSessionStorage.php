<?php

namespace Drupal\multi_session;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\multi_session\Entity\MultiSessionInterface;

/**
 * Defines the storage handler class for Multi session entities.
 *
 * This extends the base storage class, adding required special handling for
 * Multi session entities.
 *
 * @ingroup multi_session
 */
class MultiSessionStorage extends SqlContentEntityStorage implements MultiSessionStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(MultiSessionInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {multi_session_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {multi_session_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(MultiSessionInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {multi_session_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('multi_session_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
