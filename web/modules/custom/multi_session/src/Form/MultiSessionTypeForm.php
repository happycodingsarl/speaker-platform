<?php

namespace Drupal\multi_session\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class MultiSessionTypeForm.
 */
class MultiSessionTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $multi_session_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $multi_session_type->label(),
      '#description' => $this->t("Label for the Multi session type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $multi_session_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\multi_session\Entity\MultiSessionType::load',
      ],
      '#disabled' => !$multi_session_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $multi_session_type = $this->entity;
    $status = $multi_session_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Multi session type.', [
          '%label' => $multi_session_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Multi session type.', [
          '%label' => $multi_session_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($multi_session_type->toUrl('collection'));
  }

}
