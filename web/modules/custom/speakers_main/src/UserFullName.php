<?php

namespace Drupal\speakers_main;

use Drupal\Core\Field\FieldItemList;

class UserFullName extends  FieldItemList {

  public function getValue($include_computed = FALSE) {
    /** @var \Drupal\user\Entity\User $user */
    $user = $this->getEntity();
    $fields = [
      'field_prefix',
      'field_first_name',
      'field_middle_name',
      'field_last_name',
      'field_suffix',
    ];

    $row = '';
    foreach ($fields as $field) {
      $row .= $user->get($field)->isEmpty() ? '' : $user->get($field)->getString() . ' ';
    }
    return rtrim($row);
  }
}