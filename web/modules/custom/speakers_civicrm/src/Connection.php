<?php
/**
 * Created by Happy Coding ;)
 */

namespace Drupal\speakers_civicrm;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7\Request as GuzzleRequest;


/**
 * Class Connection
 *
 * @package Drupal\connection
 */
class Connection {

  /**
   * @var string  API version to use
   */
  protected $version = 'v1';

  /**
   * @var string API querying method
   */
  public $method = 'GET';

  /**
   * @var \Drupal\Core\Config\Config  settings
   */
  protected $config = NULL;

  /**
   * @var array Store sensitive API info such as the private_key & password
   */
  protected $sensitiveConfig = [];

  /**
   * Connection constructor.
   */
  public function __construct() {
    $this->config = \Drupal::config('speakers_civicrm.settings');
  }

  /**
   * Get configuration or state setting for this module.
   *
   * @param string $name this module's config or state.
   *
   * @return mixed
   */
  protected function getConfig($name) {
    $this->log("getConfig: speakers_civicrm_api_$name");

    return $this->config->get('speakers_civicrm_api_' . $name);
  }

  /**
   * Pings the  API for data.
   *
   * @param string $endpoint division endpoint to query
   * @param array $options for Url building
   *
   * @return object
   */
  public function queryEndpoint($endpoint, $options = []) {
    try {
      $response = $this->callEndpoint($endpoint, $options);
      return json_decode($response->getBody());
    } catch (\Exception $e) {
      watchdog_exception('civicrm', $e);
      return (object) [];
    }
  }

  /**
   * Call the  API endpoint.
   *
   * @param string $endpoint
   * @param array $options
   *
   * @return \Psr\Http\Message\ResponseInterface
   */
  public function callEndpoint($endpoint, $options = []) {
    $client = new GuzzleClient();
    $request = new GuzzleRequest(
      $this->method,
      $this->requestUrl($endpoint, $options),
      $this->generateHeaders($options),
      $this->generateBody($options)
    );
    return $client->send($request, ['timeout' => 300]);
  }

  /**
   * Build the URL based on the endpoint and configuration.
   *
   * @param string $endpoint to the API data
   *
   * @return string
   */
  protected function requestUrl($endpoint = '', $options = []) {
    $baseUrl = $this->getConfig('url');
    $key = $this->getConfig('key');
    if ($this->method === 'GET') {
      $url = $baseUrl . '/' . $endpoint . '/' . $key . (isset($options['id']) ? '/' . $options['id'] : '');
    }
    else{
      $url = $baseUrl . '/' . $endpoint;
    }
    $this->log($url);

    return $url;
  }

  /**
   * Build an array of headers to pass to the  API
   *
   * @param array $options
   *
   * @return array
   */
  protected function generateHeaders($options = []) {
    if ($this->method === 'GET') {
      $headers = [];
    }
    else{
      $headers = [
        'Content-Type' => 'application/json'
      ];
    }

    return $headers;
  }

  /**
   * Build the body to pass to the  API
   *
   * @param array $options
   * @return false|string|null
   */
  protected function generateBody($options = []) {
    if ($this->method === 'GET') {
      return NULL;
    }
    else{
      $key = $this->getConfig('key');
      $body = [
        'token' => $key,
      ] + $options;
      return json_encode($body);
    }
  }

  /**
   * Debug
   *
   * @param $message
   */
  private function log($message) {
    //\Drupal::messenger()->addError($message);
    //kint($message);
  }

}

