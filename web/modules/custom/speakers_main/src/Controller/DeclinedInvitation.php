<?php
namespace Drupal\speakers_main\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the dashboard demo.
 */
class DeclinedInvitation extends ControllerBase {

  /**
   * Returns a simple page dashboard demo.
   *
   * @return array
   *   A simple renderable array.
   */
  public function DeclinedInvitationView() {
    $element = array(
      '#markup' => '',
    );
    return $element;
  }

}
