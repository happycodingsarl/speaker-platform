<?php

namespace Drupal\room;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\room\Entity\RoomInterface;

/**
 * Defines the storage handler class for Room entities.
 *
 * This extends the base storage class, adding required special handling for
 * Room entities.
 *
 * @ingroup room
 */
interface RoomStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Room revision IDs for a specific Room.
   *
   * @param \Drupal\room\Entity\RoomInterface $entity
   *   The Room entity.
   *
   * @return int[]
   *   Room revision IDs (in ascending order).
   */
  public function revisionIds(RoomInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Room author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Room revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\room\Entity\RoomInterface $entity
   *   The Room entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(RoomInterface $entity);

  /**
   * Unsets the language for all Room with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
