<?php

namespace Drupal\multi_session\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Multi session type entity.
 *
 * @ConfigEntityType(
 *   id = "multi_session_type",
 *   label = @Translation("Multi session type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\multi_session\MultiSessionTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\multi_session\Form\MultiSessionTypeForm",
 *       "edit" = "Drupal\multi_session\Form\MultiSessionTypeForm",
 *       "delete" = "Drupal\multi_session\Form\MultiSessionTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\multi_session\MultiSessionTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "multi_session_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "multi_session",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/multi_session_type/{multi_session_type}",
 *     "add-form" = "/admin/structure/multi_session_type/add",
 *     "edit-form" = "/admin/structure/multi_session_type/{multi_session_type}/edit",
 *     "delete-form" = "/admin/structure/multi_session_type/{multi_session_type}/delete",
 *     "collection" = "/admin/structure/multi_session_type"
 *   }
 * )
 */
class MultiSessionType extends ConfigEntityBundleBase implements MultiSessionTypeInterface {

  /**
   * The Multi session type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Multi session type label.
   *
   * @var string
   */
  protected $label;

}
