<?php
/**
 * Created by Happy Coder
 */

namespace Drupal\speakers_scheduler\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\Renderer;

class Scheduler extends ControllerBase {

  /**
   * @var Renderer
   */
  protected $renderer;

  public function __construct(Renderer $renderer) {
    $this->renderer = $renderer;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * Get the rendered view of participants list
   *
   * @return array|null
   */
  public function fullCalendar() {

    return [
      '#theme' => 'full_calendar'
    ];
  }
}

