<?php

namespace Drupal\speakers_main\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Speaker Comment Form.
 */
class SpeakerComment extends FormBase implements ContainerInjectionInterface{

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * User, that open this page.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $user;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Creates a Speaker Comment Block instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager, MessengerInterface $messenger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'speaker_comment_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $user = NULL, $externalEventId = NULL, $token = NULL) {

    $form['message'] = [
      '#type' => 'textarea',
      //'#title' => $this->t('COMMENT ABOUT YOUR PROPOSALS AND AVAILABILITY'),
      '#maxlength' => 500,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
      '#attributes' => [
        'class' => ['btn-lg'],
        'input_text' => $this->t('Send'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    global $base_url;

    $values = $form_state->getValues();
    if (!empty($values['message'])) {

      // Emailing message to Editors.
      $this->user = $this->entityTypeManager->getStorage('user')->load(\Drupal::currentUser()->id());

      $data = [
        'message' => $values['message'],
        'fullName' => $this->user->get('full_name_computed')->getValue(),
        'lang' => $this->languageManager->getCurrentLanguage()->getId(),
        'link' => $base_url . '/user/' . \Drupal::currentUser()->id() . '/edit'
      ];
      speakers_main_email_message_editor($subject = 'Comments about my proposals', $data, 'speaker_comment');

      $this->notice($values['message']);
      $this->messenger->addMessage('Thanks for your message!');
    }
    else {
      $this->messenger->addWarning('Please fill the message.');
    }
  }

  /**
   * Log a notice
   *
   * @param $message
   */
  private function notice($message) {
    \Drupal::logger('speakers_main')->notice($message);
  }

  /**
   * Log a error
   *
   * @param $message
   */
  private function error($message) {
    \Drupal::logger('speakers_main')->error($message);
  }
}
