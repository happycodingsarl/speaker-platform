import React from 'react'
import { render } from 'react-dom'
import Calendar from './components/organisms/Calendar/Calendar'


render(<Calendar />, document.getElementById('root'))
