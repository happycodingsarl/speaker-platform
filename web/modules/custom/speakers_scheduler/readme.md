# FullCalendar webapp components
This project template provides a file and directory structure to integrate full calendar in your Drupal project with React.

## Usage
All teh webapp assets and code is in the directory /webapp you need to use npm at this root directory to build the app and compile scss.

## Default CSS styles
Add all default component style you need in the /webapp/src/components/organisms/Calendar/style.scss

## To customize your Style CSS
You should change directly in /webapp/css/style.css but we recommend to use SASS files in the /scss directory.
style.scss is the main file.

## To compile your webapp
npm run watch  
or npm run build
