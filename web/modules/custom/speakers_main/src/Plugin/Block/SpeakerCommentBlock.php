<?php


namespace Drupal\speakers_main\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;

/**
 * Provides a Block displaying speaker comment.
 *
 * @Block(
 *   id = "speaker_comment_block",
 *   admin_label = @Translation("Speaker comment"),
 *   category = @Translation("Speakers Platform"),
 * )
 */
class SpeakerCommentBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $return = \Drupal::formBuilder()->getForm('Drupal\speakers_main\Form\SpeakerComment');
    return $return;
  }
}
