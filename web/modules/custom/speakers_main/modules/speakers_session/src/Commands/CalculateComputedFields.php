<?php

namespace Drupal\speakers_session\Commands;

use Drupal\node\Entity\Node;
use Drush\Commands\DrushCommands;

/**
 * A Drush command file.
 * see: https://www.axelerant.com/resources/team-blog/how-to-write-custom-drush-9-commands-for-drupal-8
 *
 */
class CalculateComputedFields extends DrushCommands {
  /**
   * Calculate the computed fields of session.
   * see: https://www.drupal.org/project/computed_field/issues/2988058
   *
   * @command speakers_session:computed
   * @aliases s-c
   * @usage session:computed
   *   Calculate the computed fields of session.
   */
  public function computed() {
    $this->output()->writeln('Calculate the computed fields of session');

    $sessionIds = \Drupal::entityQuery('node')
      ->condition('type', 'session')
      ->execute();
    $sessions = Node::loadMultiple($sessionIds);

    foreach ($sessions as $session) {
      $field_name = 'field_abstract_counter';
      $items = $session->get($field_name);
      if (empty($items[0])) {
        $items->appendItem();
      }
      $items->preSave();
      $session->save();

      $v = $session->get('field_abstract_counter')->getValue();
      $v = json_encode($v);

      $this->output()->writeln($session->getTitle() . ' (' . $nid . ') : '.$v. 'done !');
    }
  }
}
