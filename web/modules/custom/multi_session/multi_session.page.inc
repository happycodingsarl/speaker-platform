<?php

/**
 * @file
 * Contains multi_session.page.inc.
 *
 * Page callback for Multi session entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Multi session templates.
 *
 * Default template: multi_session.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_multi_session(array &$variables) {
  // Fetch MultiSession Entity Object.
  $multi_session = $variables['elements']['#multi_session'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
