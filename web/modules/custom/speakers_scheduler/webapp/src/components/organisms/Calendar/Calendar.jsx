import Alert from "sweetalert2"
import {Col, Row} from "reactstrap"
import dayGridPlugin from '@fullcalendar/daygrid'
import FullCalendar from '@fullcalendar/react'
import interactionPlugin, {Draggable} from '@fullcalendar/interaction'
import moment from 'moment'
import momentPrecise from 'moment-precise-range-plugin'
import React from 'react'
import resourceTimelinePlugin from '@fullcalendar/resource-timeline'
import timeGridPlugin from '@fullcalendar/timegrid'
import tz from 'moment-timezone'
import * as services from '../../../services/services'
import UnscheduledSessions from "../../moleculas/UnscheduledSessions/UnscheduledSessions"

// @TODO Filter by event with group for session & multiple

// Import standard style from this full calendar components
import './style.scss'

export default class Calendar extends React.Component {

  calendarComponentRef = React.createRef()

  timezone = 'Europe/Paris'

  state = {
    calendarWeekends: false,
    calendarEvents: [],
    calendarResources: [],
    unscheduledSessions: [],
    slotWidth: '15',
    displayEventTime: true,
    slotLabelFormat: [
      {weekday: 'long', day: 'numeric', month: 'short', year: 'numeric'},// top level of text
      {
        hour: 'numeric',
        minute: '2-digit',
        omitZeroMinute: true,
        hour12: false, // true for a 12-hour clock, false for a 24-hour clock
      }, // lower level of text
    ],
    editable: true,
    eventId: '0',
    groupId: '0',
    defaultDate: '2019-09-12',
    minTime: '08:00:00',
    maxTime: '18:00:00',
    validRange: {
      start: '2020-09-01',
      end: '2020-09-05'
    }
  }

  componentDidMount() {
    this.setEventInfo().then(rep => {
      this.setRooms()
      this.setSessions(24, 0, 'unscheduledSessions')
      this.setSessions(24, 0, 'calendarEvents')
      this.setMultiSessions(24, 0)
      this.setBreaks(24, 0)

      let draggableEl = document.getElementById("external-events")
      new Draggable(draggableEl, {
        itemSelector: ".fc-event",
        eventData: function (eventEl) {
          return {
            title: eventEl.getAttribute("title"),
            id: eventEl.getAttribute("id"),
            speaker: eventEl.getAttribute("speaker"),
            category: eventEl.getAttribute("category"),
            topic: eventEl.getAttribute("topic"),
            cerp: eventEl.getAttribute("cerp")
          }
        }
      })
    })
  }

  setEventInfo = () => new Promise((resolve, reject) => {
    try {
      this.setState({
        eventId: '0',
        groupId: '0',
        defaultDate: '2019-09-12',
        validRange: {
          start: '2019-09-12',
          end: '2019-09-12'
        }
      }, () => {
        services.getEventInfo()
          .then(rep => {
            this.setState({
              eventId: rep.event.eventId,
              groupId: rep.event.groupId,
              defaultDate: rep.event.start,
              validRange: {
                start: rep.event.start,
                end: moment.tz(rep.event.end, this.timezone).add(1, 'days').format()
              }
            }, () => {
              console.log(rep, 'setEventInfo')

              // Goto Date
              let calendarApi = this.calendarComponentRef.current.getApi()
              calendarApi.gotoDate(rep.event.start) // call a method on the Calendar object

              resolve({})
            })
          })
      })
    } catch (e) {
      reject(e)
    }
  })

  /**
   * Set calendarEvents or unscheduledSessions states with sessions from API
   *
   * @param pageLimit
   * @param pageOffset
   * @param stateProperty
   */
  setSessions(pageLimit = 24, pageOffset = 0, stateProperty = 'calendarEvents') {
    let operator = 'IS NOT NULL'
    if (stateProperty === 'unscheduledSessions') {
      operator = 'IS NULL'
    }

    services.getSessions(this.state.groupId, operator, pageLimit, pageOffset)
      .then(rep => {
        this.setState({[stateProperty]: [...this.state[stateProperty], ...rep.session]})

        // Lazy loading
        if (rep.hasNextPage) {
          this.setSessions(24, pageOffset + pageLimit, stateProperty)
        } else {
          console.log(this.state[stateProperty], stateProperty, 'setSessions')
        }
      })
  }

  /**
   * Set calendarEvents state with multi sessions from API
   *
   * @param pageLimit
   * @param pageOffset
   */
  setMultiSessions(pageLimit = 24, pageOffset = 0) {

    services.getMultiSessions(pageLimit, pageOffset)
      .then(rep => {
        //console.log(rep, 'rep setMultiSessions')
        this.setState({calendarEvents: [...this.state.calendarEvents, ...rep.multiSession]})

        // Lazy loading
        if (rep.hasNextPage) {
          this.setMultiSessions(24, pageOffset + pageLimit)
        } else {
          console.log(this.state.calendarEvents, ' calendarEvents setMultiSessions')
        }
      })
  }

  /**
   * Set calendarEvents state with breaks from API
   *
   * @param pageLimit
   * @param pageOffset
   */
  setBreaks(pageLimit = 24, pageOffset = 0) {

    services.getBreaks(pageLimit, pageOffset)
      .then(rep => {
        //console.log(rep, 'rep setMultiSessions')
        this.setState({calendarEvents: [...this.state.calendarEvents, ...rep.break]})

        // Lazy loading
        if (rep.hasNextPage) {
          this.setBreaks(24, pageOffset + pageLimit)
        } else {
          console.log(this.state.calendarEvents, ' calendarEvents setBreaks')
        }
      })
  }

  setRooms() {
    try {
      this.setState({
        calendarResources: []
      }, () => {
        services.getRooms(9,40)
          .then(rep => {

              this.setState({
                calendarResources: rep.rooms.sort((a, b) => (a.code > b.code) ? 1 : -1) || []
              })
              console.log(rep.rooms.sort((a, b) => (a.code > b.code) ? 1 : -1), 'setRooms');
            }
          );
      });
    } catch (e) {
      // Pass status code as internal properly. It is being checked inside of
      // render() method of _app.js.
      //initialProps.statusCode = 500;

      // In case of Server Side rendering we want the server to throw the
      // correct error code.
      //if (res) res.statusCode = 500;Api
    }
  }

  /**
   * Add to Unscheduled Sessions
   *
   * @param id the id to search in the array
   */
  moveFromCalendarEventsToUnscheduled = (id = '') => {

    // Get session
    let calendarApi = this.calendarComponentRef.current.getApi()
    const event = calendarApi.getEventById(id)

    const session = {
      id: event.id,
      title: event.title,
      speaker: event.extendedProps.speaker,
      category: event.extendedProps.category,
      topic: event.extendedProps.topic,
      resourceId: '',
      start: '',
      end: ''
    }
    //console.log(session, 'session moveFromCalendarEventsToUnscheduled')

    // Remove from CalendarEvents
    this.removeFromProp(id)

    // Add to unscheduled
    let sessions = [...this.state.unscheduledSessions]
    sessions.push(session)

    this.setState({
      unscheduledSessions: sessions
    })
  }

  /**
   * Move From Unscheduled To CalendarEvents
   *
   * @param id the id to search in the array
   * @param startDate
   * @param endDate
   * @param resourceId
   * @param cerp
   */
  moveFromUnscheduledToCalendarEvents = (id = '', startDate = null, endDate = null, resourceId = null, cerp = null) => {
    let session = this.removeFromProp(id, 'unscheduledSessions')

    // Update the event in the calendar
    this.removeCalendarEvent(id)

    // Add the session to calendarEvents
    if (startDate != null) session.start = startDate
    if (endDate != null) session.end = endDate
    if (resourceId != null) session.resourceId = resourceId
    if (cerp != null) session.cerp = cerp

    let calendarEvents = [...this.state.calendarEvents]
    calendarEvents.push(session)
    this.setState({calendarEvents: calendarEvents})

    //console.log(this.state.calendarEvents, 'calendarEvents moveFromUnscheduledToCalendarEvents')
  }

  /**
   * Remove from props
   *
   * @param id
   * @param stateProperty
   * @returns {*}
   */
  removeFromProp = (id, stateProperty = 'calendarEvents') => {
    let calendarEvents = [...this.state[stateProperty]]

    // Find the moving session
    const movingSession = calendarEvents.find(obj => {
      return obj.id === id
    })
    //console.log(movingSession, 'movingSession moveFromUnscheduledToCalendarEvents')

    const index = calendarEvents.indexOf(movingSession)
    if (index >= 0) {
      calendarEvents.splice(index, 1)
      this.setState({
        [stateProperty]: calendarEvents
      })
    }

    return movingSession
  }

  /**
   * Remove the event in the calendar
   *
   * @param id
   */
  removeCalendarEvent = (id) => {
    let calendarApi = this.calendarComponentRef.current.getApi()
    const event = calendarApi.getEventById(id)

    if (event != null) {
      event.remove()
    }
    else{
      console.error(id, 'An error occurred when removing the calendar event!')
    }
  }

  /**
   * Check if the session is multiple
   * @DEPRECATED should be replace by this.sessionType
   *
   * @param resourceId
   * @returns {boolean}
   */
  isMultipleSession = (resourceId = '') => {
    return resourceId.slice(-9) === '-multiple'
  }

  /**
   * Get session type: multiple,break or default
   *
   * @param id
   * @param resourceId
   * @returns {string}
   */
  sessionType = (id = '', resourceId = '') => {
    return resourceId.slice(-9) === '-multiple' ? 'multiple' : id.slice(-6) === '-break' ? 'break' : 'default'
  }

  /**
   * Find sessions in the room which is planed between startDate & endDate
   *
   * @param startDate
   * @param endDate
   * @param resourceId
   * @returns {*[]} calendarEvents
   */
  findSessions = (startDate = '', endDate = '', resourceId = '') => {
    return [...this.state.calendarEvents].filter(function (session) {
      return moment(session.start).isBefore(endDate) && moment(session.end).isAfter(startDate) && session.resourceId === resourceId.substring(0, resourceId.length - 9)
    })
  }

  /**
   * Return the index of the multi session which contains the session with the eventId in param
   *
   * @param sessionId {string}
   * @returns {number}
   */
  findMultiSessionIndex = (sessionId = '') => {
    return [...this.state.calendarEvents].findIndex(o => {
      if (typeof o.sessions === 'undefined') return false

      return o.sessions.findIndex(r => r.id === sessionId) >= 0
    })
  }

  /**
   * Prepare the update calendarEvent
   *
   * @param eventId
   * @param startDate
   * @param endDate
   * @param relatedSessions
   */
  prepareUpdateCalendarEvent = (eventId = '', startDate = null, endDate = null, relatedSessions = null, cerp = null) => {
    let newScheduledSessions = [...this.state.calendarEvents]
    const index = newScheduledSessions.findIndex(o => o.id === eventId)

    if (index >= 0) {
      if (startDate != null) newScheduledSessions[index].start = startDate
      if (endDate != null) newScheduledSessions[index].end = endDate
      if (relatedSessions != null) newScheduledSessions[index].sessions = relatedSessions
      if (cerp != null) newScheduledSessions[index].cerp = cerp
      //console.log(newScheduledSessions[index], 'newScheduledSessions[index]')
    }

    return {
      calendarEvents: newScheduledSessions,
      index: index
    }
  }

  /**
   * Update a Multi Session dates from related sessions dates
   *
   * @param id string multi session id
   * @param relatedSessions [calendarEvent]
   */
  updateMultiSessionDates = (id = '', relatedSessions = []) => {
    //console.log(id, relatedSessions, 'id & relatedSessions updateMultiSessionDates')

    // The multiple session dates are set from related sessions
    const calendarEventData = this.prepareUpdateCalendarEvent(id, relatedSessions[0].start, relatedSessions[relatedSessions.length - 1].end, relatedSessions)

    // Update the Multiple Session
    services.upsertMultiSession(calendarEventData.calendarEvents[calendarEventData.index]).then(response => {
        if (response.statusCode === 200) {

          // Update calendarEvents
          this.setState({
            calendarEvents: calendarEventData.calendarEvents
          }, () => {

            // Update the event in the calendar
            let calendarApi = this.calendarComponentRef.current.getApi()
            calendarApi.getEventById(id).setDates(relatedSessions[0].start, relatedSessions[relatedSessions.length - 1].end)
          })


        } else {

          // Reset to initial dates
          console.error('An error occurred resizeSession')
        }
      }
    )
  }

  /**
   * Calculate CERP from duration
   *
   * @param startDate
   * @param endDate
   * @returns {number}
   */
  computeCerp = (startDate = null, endDate = null) => {
    let cerp = 0.01

    if (!(startDate === null)) {
      // Compute
      const duration = moment.duration(moment(endDate).diff(startDate))
      cerp = duration.asMinutes() / 60
    }
    //console.log(cerp, 'cerp')

    return cerp
  }

  /**
   * speakerAvailable
   *
   * @param speaker
   * @param startDate
   * @param endDate
   * @param id
   * @returns {boolean}
   */
  speakerAvailable = (speaker = '', startDate = '', endDate = '', id = '') => {
    //console.log(speaker, "speaker speakerAvailable")
    const conflictSessions = [...this.state.calendarEvents].filter(function (session) {
      return moment(session.start).isBefore(endDate) && moment(session.end).isAfter(startDate) && session.speaker === speaker && session.id !== id
    })
    //console.log(conflictSessions, 'conflictSessions speakerAvailable')

    if (conflictSessions.length > 0) {
      Alert.fire("Forbidden!", ' The Speaker is already scheduled at this time on ' + conflictSessions[0].title + '.', "error")

      return false
    }

    return true
  }

  /**
   * when we click on event we are displaying event details
   */
  eventClick = eventClick => {
    //console.log(eventClick, "eventClick")
    switch (this.sessionType(eventClick.event.id, eventClick.event.getResources()[0].id)) {

      case 'break':

        // Unschedule the break
        services.upsertDelBreak(null, null, eventClick.event.id, eventClick.event.getResources()[0].id)
          .then(response => {
            if (response.statusCode == 204) {
              this.removeFromProp(eventClick.event.id)
              eventClick.event.remove()
            }
          })
        break

      case 'multiple':

        // Redirect to multi session edit page
        window.location.assign('/admin/structure/multi_session/' + eventClick.event.extendedProps.eid + '/edit?destination=/admin/speakers/scheduler')
        break

      default:

        // Display a popup
        let event = eventClick.event
        let eventStart = moment.tz(event.start, this.timezone)
        let eventEnd = moment.tz(event.end, this.timezone)
        let durationHour = moment.preciseDiff(eventEnd, eventStart)

        // Find the multi session related
        const multiSessionIndex = this.findMultiSessionIndex(eventClick.event.id)
        let relatedSessions = []
        let multiCERP = 0
        let multiCERPDisplay = ''

        if (multiSessionIndex >= 0) {

          // Get related sessions by the multiple session in which the session to unschedule is
          const relatedSessionsIds = [...this.state.calendarEvents][multiSessionIndex].sessions.filter(session => session.id !== eventClick.event.id).map(session => session.id)
          relatedSessions = [...this.state.calendarEvents].filter(session => relatedSessionsIds.indexOf(session.id) >= 0)

          // Compute CERP for multi-session
          multiCERP += Number(event.extendedProps.cerp)
          relatedSessions.forEach(function (session) {
              multiCERP += Number(session.cerp)
            }
          )
          multiCERPDisplay =
            `<tr >
        <td>Multi-session CERP:</td>
      <td><span className="event--multicerp">
      ` +
            multiCERP +
            `
      </span></td>
      </tr>`
        }

        Alert.fire({
          title: event.title,
          html:
            `<div class="table-responsive">
      <table class="table">
      <tbody>
      <tr >
        <td>Category:</td>
        <td><span className="label label--category">` +
            event.extendedProps.category +
            `</span></td>
      </tr>
      <tr >
        <td>Topic:</td>
        <td><span className="label label--topic">` +
            event.extendedProps.topic +
            `
        </span></td>
      </tr>
      <tr >
        <td>Speaker:</td>
        <td><span className="event--speaker">
        ` +
            event.extendedProps.speaker +
            `
        </span></td>
      </tr>
      <tr >
        <td>Scheduled time:</td>
        <td><span>
        ` +
            eventStart.format('H:mm') + ' to ' + eventEnd.format('H:mm') +
            `
        </span></td>
      </tr>
      <tr >
        <td>Duration:</td>
        <td><span>
        ` +
            durationHour +
            `
        </span></td>
      </tr>
      <tr >
        <td>CERP:</td>
        <td><span className="event--cerp">
        ` +
            event.extendedProps.cerp +
            `
        </span></td>
      </tr>
      ` +
            multiCERPDisplay +
            `
      </tbody>
      </table>
      </div>`,

          showCloseButton: true,
          showCancelButton: true,
          confirmButtonColor: "#d33",
          cancelButtonColor: "#3085d6",
          confirmButtonText: "Unschedule",
          cancelButtonText: "Cancel"
        }).then(result => {
          if (result.value) {

            // Unschedule a session
            let updateShouldBeDone = false

            if (multiSessionIndex >= 0) {
              if ([...this.state.calendarEvents][multiSessionIndex].sessions.length === 1) {

                // Prevent remove a session linked to a multi session which contains only one session
                Alert.fire("Forbidden!", "Please remove the multi session first.", "error")
              } else {

                // Update Multi session dates
                this.updateMultiSessionDates(this.state.calendarEvents[multiSessionIndex].id, relatedSessions)

                updateShouldBeDone = true
              }
            } else {
              updateShouldBeDone = true
            }

            if (updateShouldBeDone) {
              const cerp = this.computeCerp()

              // Unscheduled a session
              services.updateSession(eventClick.event.id, null, null, null, cerp)
                .then(response => {
                  this.moveFromCalendarEventsToUnscheduled(eventClick.event.id)
                  eventClick.event.remove()
                })
            }
          }
        })
    }
  }

  /**
   * handle drop session from unscheduled to calendar
   *
   * https://fullcalendar.io/docs/eventReceive
   * @param info
   */
  handleEventReceive = (info) => {
    //console.log(info,'handleEventReceive');
    const startDate = moment.tz(info.event.start, this.timezone).format()
    const endDate = moment.tz(info.event.start, this.timezone).add(1, 'hours').format()
    const cerp = this.computeCerp(startDate, endDate)

    // Check if speaker is available & if the session is put on the right line, not multi-session line
    if (this.speakerAvailable(info.draggedEl.attributes.speaker.nodeValue, startDate, endDate, info.draggedEl.id)
      && !this.isMultipleSession(info.event.getResources()[0].id)) {
      services.updateSession(info.draggedEl.id, startDate, endDate, info.event.getResources()[0].id, cerp)
        .then(response => {
          //console.log(response, 'response handleDrop')
          if (response.statusCode === 200) {
            this.moveFromUnscheduledToCalendarEvents(info.draggedEl.id, startDate, endDate, info.event.getResources()[0].id, cerp)
            console.log(info.draggedEl.id, 'session updated!')
          } else {
            console.error(info.draggedEl.id, 'session not updated!')
            this.removeCalendarEvent(info.draggedEl.id)
          }
        })
    } else {
      console.warn(info.draggedEl.id, 'session not updated because the speaker is not available or session not put on the right line!')
      this.removeCalendarEvent(info.draggedEl.id)
    }
  }

  /**
   * Handle duration change on event
   * Triggered when resizing stops and the event has changed in duration.
   * see: https://fullcalendar.io/docs/eventResize
   *
   * @param eventResizeInfo
   */
  handleResize = (eventResizeInfo) => {
    //console.log(eventResizeInfo, 'handleResize')
    this.resizeSession(eventResizeInfo);
  }

  /**
   * Handle moved event
   * Triggered when dragging stops and the event has moved to a different day/time.
   * see: https://fullcalendar.io/docs/eventDrop
   *
   * @param eventDropInfo
   */
  handleDropInfo = (eventDropInfo) => {
    //console.log(eventDropInfo, 'handleDropInfo')
    const multiple = this.isMultipleSession(eventDropInfo.event.getResources()[0].id)
    const relatedSessions = typeof eventDropInfo.event.extendedProps.sessions != 'undefined'

    // Check if the move concerns a session on the same line only
    if (!multiple && !relatedSessions) {
      this.resizeSession(eventDropInfo);
    } else {
      eventDropInfo.revert()
      Alert.fire("Forbidden!", "You can move only a session on the same line.", "error")
    }
  }

  /**
   * Resize a session
   *
   * @param DropInfo
   */
  resizeSession = (DropInfo) => {
    //console.log(DropInfo, 'DropInfo')
    const startDate = moment.tz(DropInfo.event.start, this.timezone).format()
    const endDate = DropInfo.event.end != null ? moment.tz(DropInfo.event.end, this.timezone).format() : null
    let calendarEventData = {}

    switch (this.sessionType(DropInfo.event.id, DropInfo.event.getResources()[0].id)) {

      case 'break':
        //console.log(DropInfo.event.id.substring(0, DropInfo.event.id.length - 6), "break")
        services.upsertDelBreak(startDate, endDate, DropInfo.event.id, DropInfo.event.getResources()[0].id).then(rep => {
          if (rep.statusCode === 200) {

            // Update calendarEvents
            calendarEventData = this.prepareUpdateCalendarEvent(DropInfo.event.id, startDate, endDate)
            this.setState({calendarEvents: calendarEventData.calendarEvents})
          }
        })
        break

      case 'multiple':

        // Resize the related multiple session and link it to the related sessions.

        // Find the related sessions
        const relatedSessions = this.findSessions(DropInfo.event.start, DropInfo.event.end, DropInfo.event.getResources()[0].id)
        //console.log(relatedSessions, 'relatedSessions resizeSession')

        // Update Multi session dates
        this.updateMultiSessionDates(DropInfo.event.id, relatedSessions)
        break

      default:

        // Update the session
        const resourceId = typeof DropInfo.event.getResources() != null ? DropInfo.event.getResources()[0].id : null

        // Keep user input 0 for CERP value
        let cerp = 0
        if (DropInfo.event.extendedProps.cerp != 0) {
          cerp = this.computeCerp(startDate, endDate)
        }

        // Update calendarEvents : The multiple session dates are set from related sessions
        calendarEventData = this.prepareUpdateCalendarEvent(DropInfo.event.id, startDate, endDate, null, cerp)
        this.setState({calendarEvents: calendarEventData.calendarEvents})

        // Update the event in the calendar
        let calendarApi = this.calendarComponentRef.current.getApi()
        calendarApi.getEventById(DropInfo.event.id).setExtendedProp('cerp', cerp)

        // Check if speaker is available
        if (this.speakerAvailable(DropInfo.event.extendedProps.speaker, startDate, endDate, DropInfo.event.id)) {
          services.updateSession(DropInfo.event.id, startDate, endDate, resourceId, cerp).then(response => {
              if (response.statusCode === 200) {

                // Find the multi session related
                const multiSessionIndex = this.findMultiSessionIndex(DropInfo.event.id)

                if (multiSessionIndex >= 0) {

                  // Get related sessions by the multiple session in which the session to unschedule is
                  const relatedSessionsIds = [...this.state.calendarEvents][multiSessionIndex].sessions.map(session => session.id)
                  const relatedSessions = [...this.state.calendarEvents].filter(session => relatedSessionsIds.indexOf(session.id) >= 0)
                  //console.log(relatedSessions, 'relatedSessions')

                  // Update Multi session dates
                  this.updateMultiSessionDates(this.state.calendarEvents[multiSessionIndex].id, relatedSessions)
                }
              }
            }
          )
        }
        else{
          DropInfo.revert()
        }
    }
  }

  slotZoomIn = () => {
    var width = this.state.slotWidth * 1.5;
    this.setState({
      slotWidth: width
    })
  }

  slotZoomOut = () => {
    var width = this.state.slotWidth / 1.5;
    this.setState({
      slotWidth: width
    })
  }

  EventDetail = ({event, el}) => {
    const content = (
      <div className="event">
        <div>
          <span className="label label--category">{event.extendedProps.category}</span><br />
          <span className="event--title">{event.title}</span>
          <span className="event--speaker">{event.extendedProps.speaker}</span>
        </div>
        <div className="fc-resizer fc-start-resizer"></div>
        <div className="fc-resizer fc-end-resizer"></div>
      </div>
    );
    ReactDOM.render(content, el);
    return el;
  }

  /**
   * handle Date Click
   * Create a new multiple session or a break
   * see https://fullcalendar.io/docs/dateClick
   *
   * @param info
   */
  handleDateClick = (info) => {
    //console.log(info, 'info handleDateClick')

    if (this.isMultipleSession(info.resource.id)) {

      // Handle click on room line

      // Find the sessions in the same room at this date
      let events = this.findSessions(info.dateStr, moment.tz(info.dateStr, this.timezone).add(15, 'minutes').format(), info.resource.id)

      // Create a new calendar event
      if (events.length === 1) {
        const event = [{
          id: null,
          title: events[0].title,
          resourceId: info.resource.id,
          start: events[0].start,
          end: events[0].end,
          sessions: [{id: events[0].id}]
        }]

        // Create a multi session
        services.upsertMultiSession(event[0]).then(response => {
            if (response.statusCode === 201) {

              // Create calendar event
              event[0].id = response.id
              event[0].eid = response.eid
              //console.log(event, 'create multiple session')

              this.setState({calendarEvents: [...this.state.calendarEvents, ...event]})
              //console.log(this.state.calendarEvents, 'this.state.calendarEvents')
            } else {

              // Remove event if error
              this.removeCalendarEvent(info.resource.id)
            }
          }
        )
      }
    } else {

      // Handle click on session line to create a break
      const endDate = moment.tz(info.dateStr, this.timezone).add(1, 'hours').format()

      services.upsertDelBreak(info.dateStr, endDate, null, info.resource.id).then(rep => {
        if (rep.statusCode === 201) {

          // Create a new calendar event
          const event = [{
            id: rep.id + '-break',
            title: 'Break',
            resourceId: info.resource.id,
            start: info.dateStr,
            end: endDate
          }]

          this.setState({calendarEvents: [...this.state.calendarEvents, ...event]})
        } else {

          // Remove event if error
          this.removeCalendarEvent(info.resource.id)
        }
      })
    }
  }

  render() {
    return (
      <div className='demo-app'>
        <div className='calendar'>
          <Row>
            <Col className='first'>
              <UnscheduledSessions
                events={this.state.unscheduledSessions}
              />
            </Col>

            <Col>
              <FullCalendar
                defaultView="resourceTimelineDay"
                resourceLabelText='Rooms'
                resourceAreaWidth='150px'
                slotWidth={this.state.slotWidth}
                slotDuration='00:15'
                slotLabelInterval='01:00'
                header={{
                  left: 'prev,next slotZoomOut slotZoomIn',
                  center: 'title',
                  right: '' // for day view resourceTimelineDay, for week view resourceTimelineWeek...
                }}
                plugins={[resourceTimelinePlugin, dayGridPlugin, timeGridPlugin, interactionPlugin]}
                schedulerLicenseKey='0565193978-fcs-1571835679'
                ref={this.calendarComponentRef}
                weekends={this.state.calendarWeekends}
                events={this.state.calendarEvents}
                eventRender={this.EventDetail}
                resources={this.state.calendarResources}
                dateClick={this.handleDateClick}
                defaultDate={this.state.defaultDate}
                editable={this.state.editable}
                eventResize={this.handleResize}
                eventDrop={this.handleDropInfo}
                eventReceive={this.handleEventReceive}
                eventClick={this.eventClick}
                minTime={this.state.minTime}
                maxTime={this.state.maxTime}
                validRange={this.state.validRange}
                customButtons={{
                  slotZoomIn: {
                    text: 'Zoom In',
                    click: this.slotZoomIn
                  },
                  slotZoomOut: {
                    text: 'Zoom out',
                    click: this.slotZoomOut
                  }
                }}
              />
            </Col>
          </Row>
        </div>
      </div>
    )
  }
}
