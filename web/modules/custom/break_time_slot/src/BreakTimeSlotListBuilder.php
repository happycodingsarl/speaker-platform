<?php

namespace Drupal\break_time_slot;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Break time slot entities.
 *
 * @ingroup break_time_slot
 */
class BreakTimeSlotListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Break time slot ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\break_time_slot\Entity\BreakTimeSlot $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.break_time_slot.edit_form',
      ['break_time_slot' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
