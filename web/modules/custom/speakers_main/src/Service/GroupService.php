<?php

namespace Drupal\speakers_main\Service;

use Drupal\group\Entity\GroupContent;

/**
 * Class GroupService.
 */
class GroupService {

  /**
   * Get groupId for the current selected event
   *
   * @return string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getEventGroupId() {

    // Get event id
    $session = \Drupal::request()->getSession();
    $eventId = (int) $session->get('selectedEvent', 0);

    // For publisher set the default event
    $roles = \Drupal::currentUser()->getRoles();
    if (empty($eventId) && in_array('publisher', $roles)) {
      if (!$eventId = $this->getDefaultEvent()) {
        \Drupal::logger('speakers_main')->error('Default event not found in the method getDefaultEvent of GroupService.');
      }
    }

    // Get group id
    $data = [
      'type' => 'default',
      'id' => $eventId,
    ];
    $event = \Drupal::entityTypeManager()
      ->getStorage('event')
      ->create($data);
    $group_contents = GroupContent::loadByEntity($event);
    $group_content = reset($group_contents);

    $groupId = '';
    if ($group_content) {
      $groupId = $group_content->getGroup()->id();
    }

    return $groupId;
  }

  /**
   * Get a default event
   * Build only for publisher role
   *
   * @return bool|array
   */
  private function getDefaultEvent() {

    // List
    $query = \Drupal::entityQuery('event')->sort('created', 'DESC');
    $eventIds = $query->execute();
    if (is_array($eventIds)) {
      return array_shift($eventIds);
    }
    else{
      return false;
    }
  }
}
