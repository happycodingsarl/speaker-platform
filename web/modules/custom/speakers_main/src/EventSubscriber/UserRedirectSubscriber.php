<?php

namespace Drupal\speakers_main\EventSubscriber;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Class UserRedirectSubscriber.
 * Send the publisher to the final program speaker photos page instead of user page.
 *
 * @package Drupal\speaker_main\EventSubscriber
 */
class UserRedirectSubscriber implements EventSubscriberInterface {

  /**
   * The path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs.
   *
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   The path matcher service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user.
   */
  public function __construct(PathMatcherInterface $path_matcher, AccountInterface $current_user) {
    $this->pathMatcher = $path_matcher;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['initData'];
    return $events;
  }

  /**
   * Manage the redirect logic.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   Managed event.
   */
  public function initData(GetResponseEvent $event) {

    // Make sure front page module is not run when using cli (drush).
    // Make sure front page module does not run when installing Drupal either.
    if (PHP_SAPI === 'cli' || drupal_installation_attempted()) {
      return;
    }

    // Don't run when site is in maintenance mode.
    if (\Drupal::state()->get('system.maintenance_mode')) {
      return;
    }

    // Ignore non index.php requests (like cron).
    if (!empty($_SERVER['SCRIPT_FILENAME']) && realpath(DRUPAL_ROOT . '/index.php') != realpath($_SERVER['SCRIPT_FILENAME'])) {
      return;
    }

    // Send the publisher to the final program speaker photos page instead of user page.
    $route_name = \Drupal::routeMatch()->getRouteName();
    $userRoles = $this->currentUser->getRoles();
    if ($route_name == 'entity.user.canonical' && in_array('publisher', $userRoles)) {//entity.user.canonical user.page user.login
      $event->setResponse(new RedirectResponse('/admin/speakers/photos'));
    }
  }
}
