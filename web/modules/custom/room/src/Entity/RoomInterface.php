<?php

namespace Drupal\room\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Room entities.
 *
 * @ingroup room
 */
interface RoomInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Room name.
   *
   * @return string
   *   Name of the Room.
   */
  public function getName();

  /**
   * Sets the Room name.
   *
   * @param string $name
   *   The Room name.
   *
   * @return \Drupal\room\Entity\RoomInterface
   *   The called Room entity.
   */
  public function setName($name);

  /**
   * Gets the Room creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Room.
   */
  public function getCreatedTime();

  /**
   * Sets the Room creation timestamp.
   *
   * @param int $timestamp
   *   The Room creation timestamp.
   *
   * @return \Drupal\room\Entity\RoomInterface
   *   The called Room entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Room revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Room revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\room\Entity\RoomInterface
   *   The called Room entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Room revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Room revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\room\Entity\RoomInterface
   *   The called Room entity.
   */
  public function setRevisionUserId($uid);

}
