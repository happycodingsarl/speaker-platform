<?php

namespace Drupal\room\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Room type entity.
 *
 * @ConfigEntityType(
 *   id = "room_type",
 *   label = @Translation("Room type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\room\RoomTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\room\Form\RoomTypeForm",
 *       "edit" = "Drupal\room\Form\RoomTypeForm",
 *       "delete" = "Drupal\room\Form\RoomTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\room\RoomTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "room_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "room",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/room_type/{room_type}",
 *     "add-form" = "/admin/structure/room_type/add",
 *     "edit-form" = "/admin/structure/room_type/{room_type}/edit",
 *     "delete-form" = "/admin/structure/room_type/{room_type}/delete",
 *     "collection" = "/admin/structure/room_type"
 *   }
 * )
 */
class RoomType extends ConfigEntityBundleBase implements RoomTypeInterface {

  /**
   * The Room type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Room type label.
   *
   * @var string
   */
  protected $label;

}
