<?php

namespace Drupal\speakers_header\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\file\Entity\File;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Provides a 'SpeakersHeaderBlock' block.
 *
 * @Block(
 *  id = "speakers_header_block",
 *  admin_label = @Translation("Speakers header block"),
 * )
 */
class SpeakersHeaderBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Symfony\Component\DependencyInjection\ContainerAwareInterface definition.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerAwareInterface
   */
  protected $entityQuery;

  /**
   * The entity manager
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Active request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  public $request;

  /**
   * Drupal\Core\Render\RendererInterface definition
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * SpeakersHeaderBlock constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param \Symfony\Component\DependencyInjection\ContainerAwareInterface $entity_query
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Render\RendererInterface $renderer
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Request $request, ContainerAwareInterface $entity_query, EntityTypeManagerInterface $entity_type_manager, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->request = $request;
    $this->entityQuery = $entity_query;
    $this->entityTypeManager = $entity_type_manager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('entity.query'),
      $container->get('entity_type.manager'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $session = $this->request->getSession();

    if (!$this->entityTypeManager->hasDefinition('event')) {
      return $build;
    }

    $eids = speaker_header_get_events_list();
    $events_storage = $this->entityTypeManager->getStorage('event');
    $event_selected = $session->has('selectedEvent') ? $session->get('selectedEvent') : current($eids);
    $event = $events_storage->load($event_selected);

    if (empty($event)) {
      return $build;
    }

    $current_event['title'] = $event->name->value . ' - ' .
      $event->field_city->value . ' / ' .
      $event->field_country->value;
    
    if (!$event->status->value) {
      $current_event['title'] .= ' (' . t('Unpublished') . ')';
    }

    if (is_null($event->field_banner_image->target_id)) {
      return $build;
    }
    $file = File::load($event->field_banner_image->target_id);

    if ($file) {
      $image = \Drupal::service('image.factory')->get($file->getFileUri());
      if ($image->isValid()) {
        $width = $image->getWidth();
        $height = $image->getHeight();
      }
      else {
        $width = $height = NULL;
      }
      if ($image->isValid()) {
        $build['event'] = [
          '#type' => 'container',
          '#prefix' => '<div id="event-image-container">',
          '#suffix' => '</div>',
        ];
        /*
        $build['event']['title'] = [
          '#markup' => $current_event['title'],
          '#prefix' => '<div id="event-title">',
          '#suffix' => '</div>',
        ];*/
        $build['event']['image'] = [
          '#theme' => 'image_style',
          '#width' => $width,
          '#height' => $height,
          '#style_name' => 'large',
          '#uri' => $file->getFileUri(),
          '#prefix' => '<div id="event-image">',
          '#suffix' => '</div>',
        ];
      }

    }
    // Need to invalidate block caching in depend on $session.
    $this->renderer->addCacheableDependency($build, $session);

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // No caching. Just to be sure to update block for Speakers.
    return 0;
  }

}
