<?php
/**
 * Created by Happy coder ;)
 */

namespace Drupal\speakers_main\Plugin\GroupContentEnabler;

use Drupal\Component\Plugin\Derivative\DeriverBase;

class EventEntityDeriver extends DeriverBase {

  /**
   * {@inheritdoc}.
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives['default'] = [
        'entity_bundle' => 'default',
        'label' => t('Group event (default)'),
        'description' => t('Adds an event to groups.'),
      ] + $base_plugin_definition;
    return $this->derivatives;
  }
}
