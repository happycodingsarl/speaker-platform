import request from './request';
import * as transforms from './transforms';
import moment from "moment";

const JsonApiPrefix = '/jsonapi'
let token = ''

/**
 * Returns all sessions from the backend.
 * Manage unscheduled and scheduled sessions ('IS NOT NULL')
 *
 * @param groupId
 * @param operator
 * @param pageLimit
 * @param pageOffset
 * @returns {Promise<unknown>}
 */
export const getSessions = (groupId = 0, operator = 'IS NOT NULL', pageLimit = 24, pageOffset = 0) => new Promise((resolve, reject) => {
  let query = {
    'fields[group_content--participant-group_node-session]': 'entity_id',
    'fields[node--session]': 'title,field_scheduled_date,field_room,field_participant,field_language,field_title,field_session_status,field_session_category,field_session_topic,field_session_cerp',
    'fields[user--user]': 'field_first_name,field_last_name',
    'include': 'entity_id,entity_id.field_participant,entity_id.field_session_category,entity_id.field_session_topic,gid',
    'filter[group_id][condition][path]': 'gid.drupal_internal__id',
    'filter[group_id][condition][operator]': '=',
    'filter[group_id][condition][value]': groupId,
    'filter[date][condition][path]': 'entity_id.field_scheduled_date.value',
    'filter[date][condition][operator]': operator,
    'filter[session_status][condition][path]': 'entity_id.field_session_status',
    'filter[session_status][condition][operator]': 'IN',
    'filter[session_status][condition][value][1]': 'accepted',
    'filter[session_status][condition][value][2]': 'correctionNeeded',
    'sort': 'entity_id.field_participant.field_last_name,entity_id.field_title',
    'page[limit]': pageLimit,
    'page[offset]': pageOffset,
  }

  // Sorting
  if (operator === 'IS NOT NULL') {
    query['sort'] = 'entity_id.field_scheduled_date.value'
  }
  //console.log(query);

  request
    .get(JsonApiPrefix + '/group_content/participant-group_node-session')// node/session
    .query(query)
    // Tell superagent to consider any valid Drupal response as successful.
    // Later we can capture error codes if needed.
    .ok(resp => resp.statusCode)
    .then((response) => {
      resolve({
        session: response.statusCode === 200 ? response.body.data.map(session => transforms.session(session)) : [],
        hasNextPage: !!response.body.links.next,
        hasPrevPage: !!response.body.links.prev,
        statusCode: response.statusCode,
      })
    })
    .catch((error) => {
      console.error('Could not fetch list of sessions.', error);
      reject(error)
    })
});

/**
 * Update a session.
 *
 * @param uuid
 * @param startDate
 * @param endDate
 * @param resourceId
 * @param cerp
 * @returns {Promise<unknown>}
 */
export const updateSession = (uuid = '', startDate = null, endDate = null, resourceId = null, cerp = null) => new Promise((resolve, reject) => {

  let body =
    {
      "data": {
        "type": "node--session",
        "id": uuid,
        "attributes": {},
        "relationships": {}
      },
    }

  // Dates
  body.data.attributes = {
    "field_scheduled_date": {
      "value": startDate,//"2019-09-10T09:00:00+00:00",
      "end_value": endDate //"2019-09-10T11:00:00+00:00"
    },
    "field_session_cerp": cerp
  }

  // Room
  let fieldRoom = null
  if (resourceId != null) {
    fieldRoom = {
      "data": {
        "type": "room--default",
        "id": resourceId
      }
    }
  }
  body.data.relationships = {
    "field_room": fieldRoom
  }

  getToken()
    .then(rep => {
      token = rep.token
      console.log(token, 'token')
      request
        .set('X-CSRF-Token', token)
        .patch(JsonApiPrefix + '/node/session/' + uuid)
        .send(body)
        // Tell superagent to consider any valid Drupal response as successful.
        // Later we can capture error codes if needed.
        .ok(resp => resp.statusCode)
        .then((response) => {
          resolve({
            statusCode: response.statusCode,
          });
        })
        .catch((error) => {
          console.error('Could not patch the session.', error);
          reject(error);
        })
    })
})

/**
 * Returns all Multi sessions from the backend.
 *
 * @param pageLimit
 * @param pageOffset
 * @returns {Promise<unknown>}
 */
export const getMultiSessions = (pageLimit = 24, pageOffset = 0) => new Promise((resolve, reject) => {
  let query = {
    'fields[multi_session--default]': 'name,field_cerp,field_chair,field_session,drupal_internal__id',
    'fields[node--session]': 'field_scheduled_date,field_room',
    'include': 'field_session',
    'sort': 'field_session.field_scheduled_date.value',
    'page[limit]': pageLimit,
    'page[offset]': pageOffset,
  }
  //console.log(query);

  request
    .get(JsonApiPrefix + '/multi_session/default')
    .query(query)
    // Tell superagent to consider any valid Drupal response as successful.
    // Later we can capture error codes if needed.
    .ok(resp => resp.statusCode)
    .then((response) => {
      //console.log(response, 'response getMultiSessions')
      resolve({
        multiSession: response.statusCode === 200 ? response.body.data.map(multiSession => transforms.multiSession(multiSession)) : [],
        hasNextPage: !!response.body.links.next,
        hasPrevPage: !!response.body.links.prev,
        statusCode: response.statusCode,
      })
    })
    .catch((error) => {
      console.error('Could not fetch list of sessions.', error);
      reject(error);
    });
});

/**
 * Returns all Multi sessions from the backend.
 *
 * @param pageLimit
 * @param pageOffset
 * @returns {Promise<unknown>}
 */
export const getBreaks = (pageLimit = 24, pageOffset = 0) => new Promise((resolve, reject) => {
  let query = {
    'fields[break_time_slot--break_time_slot]': 'field_date,field_room',
    'sort': 'field_date.value',
    'page[limit]': pageLimit,
    'page[offset]': pageOffset,
  }
  //console.log(query);

  request
    .get(JsonApiPrefix + '/break_time_slot/break_time_slot')
    .query(query)
    // Tell superagent to consider any valid Drupal response as successful.
    // Later we can capture error codes if needed.
    .ok(resp => resp.statusCode)
    .then((response) => {
      //console.log(response, 'response getBreaks')
      resolve({
        break: response.statusCode === 200 ? response.body.data.map(breakTimeSlot => transforms.breakTimeSlot(breakTimeSlot)) : [],
        hasNextPage: !!response.body.links.next,
        hasPrevPage: !!response.body.links.prev,
        statusCode: response.statusCode,
      })
    })
    .catch((error) => {
      console.error('Could not fetch list of sessions.', error);
      reject(error);
    });
});

/**
 * Update a session.
 *
 * @param event = {id: '', title: '', resourceId: '', start: '', end: '', sessions: ''}
 * @returns {Promise<unknown>}
 */
export const upsertMultiSession = (event) => new Promise((resolve, reject) => {

  let body =
    {
      "data": {
        "type": "multi_session--default",
        "attributes": {},
        "relationships": {
          "field_session": {
            "data": []
          }
        }
      }
    }

  // Title
  if (event.title != null) {
    body.data.attributes = {
      "name": event.title
    }
  }

  // Id
  if (event.id != null) {
    body.data.id = event.id
  }

  // Related sessions
  if (event.sessions != null) {
    event.sessions.forEach(function (session) {
        //console.log(session, 'session updateAddMultiSession');
        body.data.relationships.field_session.data.push({
          "type": "node--session",
          "id": session.id
        })
      }
    )
  }
  //console.log(body, 'body')

  getToken()
    .then(rep => {
      token = rep.token
      console.log(token, 'token')

      if (event.id != null) {

        // Update a multiple session
        request
          .set('X-CSRF-Token', token)
          .patch(JsonApiPrefix + '/multi_session/default/' + event.id)
          .send(body)
          .ok(resp => resp.statusCode)
          .then((response) => {
            //console.log(response, 'response update upsertMultiSession')
            resolve({
              statusCode: response.statusCode,
              id: response.body.data != null ? response.body.data.id : ''
            })
          })
          .catch((error) => {
            console.error('Could not update the multi session.', error)
            reject(error)
          })
      } else {

        // Add a new multiple session
        request
          .post(JsonApiPrefix + '/multi_session/default')
          .send(body)
          .set('X-CSRF-Token', token)
          .ok(resp => resp.statusCode)
          .then((response) => {
            //console.log(response, 'response add upsertMultiSession')
            resolve({
              statusCode: response.statusCode,
              id: response.body.data != null ? response.body.data.id : '',
              eid: response.body.data != null ? response.body.data.drupalInternalId : ''
            })
          })
          .catch((error) => {
            console.error('Could not add the multi session.', error)
            reject(error)
          })
      }
    })
})

/**
 * Delete, Update or insert a break.
 *
 * @param event = {id: '', title: '', resourceId: '', start: '', end: '', sessions: ''}
 * @returns {Promise<unknown>}
 */
export const upsertDelBreak = (startDate = null, endDate = null, id = null, resourceId = null) => new Promise((resolve, reject) => {

  let body =
    {
      "data": {
        "type": "break_time_slot--break_time_slot",
        "attributes": {},
        "relationships": {
          "field_room": {
            "data": []
          }
        }
      }
    }

  body.data.attributes = {
    "field_date": {
      "value": startDate,//"2019-09-10T09:00:00+00:00",
      "end_value": endDate //"2019-09-10T11:00:00+00:00"
    },
    "name": startDate
  }

  // Id
  if (id != null) {
    id = id.substring(0, id.length - 6)
    body.data.id = id
  }

  // Related room
  if (resourceId != null) {
    body.data.relationships.field_room.data.push({
      "type": "room--default",
      "id": resourceId
    })
  }
  //console.log(body, 'body')

  getToken()
    .then(rep => {
      token = rep.token
      console.log(token, 'token')

      if (id != null) {
        if (startDate != null) {

          // Update
          request
            .set('X-CSRF-Token', token)
            .patch(JsonApiPrefix + '/break_time_slot/break_time_slot/' + id)
            .send(body)
            .ok(resp => resp.statusCode)
            .then((response) => {
              //console.log(response, 'response update')
              resolve({
                statusCode: response.statusCode
              })
            })
            .catch((error) => {
              console.error('Could not update the break.', error)
              reject(error)
            })
        } else {

          // Delete
          request
            .set('X-CSRF-Token', token)
            .delete(JsonApiPrefix + '/break_time_slot/break_time_slot/' + id)
            .ok(resp => resp.statusCode)
            .then((response) => {
              //console.log(response, 'response update')
              resolve({
                statusCode: response.statusCode
              })
            })
            .catch((error) => {
              console.error('Could not update the break.', error)
              reject(error)
            })

        }
      } else {

        // Add
        request
          .post(JsonApiPrefix + '/break_time_slot/break_time_slot')
          .send(body)
          .set('X-CSRF-Token', token)
          .ok(resp => resp.statusCode)
          .then((response) => {
            //console.log(response, 'response add break')
            resolve({
              statusCode: response.statusCode,
              id: response.body.data != null ? response.body.data.id : ''
            })
          })
          .catch((error) => {
            console.error('Could not add the break.', error)
            reject(error)
          })
      }
    })
})

export const getToken = () => new Promise((resolve, reject) => {

  // Check if the token is set
  if (token === "") {
    request
      .get('/session/token')
      //.query(body)
      // Tell superagent to consider any valid Drupal response as successful.
      // Later we can capture error codes if needed.
      .ok(resp => resp.statusCode)
      .then((response) => {
        resolve({
          token: response.text,
          statusCode: response.statusCode
        });
      })
      .catch((error) => {
        console.error('Could not get the token.', error);
        reject(error);
      });
  } else {
    resolve({
      token: token
    })
  }
})

export const getEventInfo = () => new Promise((resolve, reject) => {

  getToken()
    .then(rep => {
      token = rep.token
      console.log(token, 'token')
      request
        .set('X-CSRF-Token', token)
        .get('/speakers-api/v1/get.json')
        // Tell superagent to consider any valid Drupal response as successful.
        // Later we can capture error codes if needed.
        .ok(resp => resp.statusCode)
        .then((response) => {
          resolve({
            event: response.statusCode === 200 ? transforms.event(response.body.data) : [],
            statusCode: response.statusCode,
          })
        })
        .catch((error) => {
          console.error('Could not get Event Info.', error);
          reject(error);
        })
    })
})

/**
 * get Rooms by group
 *
 * example:
 * http://speaker.localhost:8000/jsonapi/group_content/group_content_type_ca93c7a2d7738?include=gid&filter[g][condition][path]=gid.drupal_internal__id&filter[g][condition][operator]=%3D&filter[g][condition][value]=9
 *
 * @param groupId
 * @param pageLimit
 * @param pageOffset
 * @returns {Promise<unknown>}
 */
export const getRooms = (groupId = 9, pageLimit = 24, pageOffset = 0) => new Promise((resolve, reject) => {
  request
    .get(JsonApiPrefix + '/group_content/group_content_type_ca93c7a2d7738')
    .query({
      'fields[group_content--group_content_type_ca93c7a2d7738]': 'entity_id,gid',
      'include': 'entity_id,gid',
      'filter[group_id][condition][path]': 'gid.drupal_internal__id',
      'filter[group_id][condition][operator]': '=',
      'filter[group_id][condition][value]': groupId,
      'sort': 'entity_id.field_code,entity_id.name',
      'page[limit]': pageLimit,
      'page[offset]': pageOffset,
    })

    // Tell superagent to consider any valid Drupal response as successful.
    // Later we can capture error codes if needed.
    .ok(resp => resp.statusCode)
    .then((response) => {
      // console.log(response, 'response room')
      resolve({
        rooms: response.statusCode === 200 ? response.body.data.map(room => transforms.room(room)) : [],
        hasNextPage: !!response.body.links.next,
        hasPrevPage: !!response.body.links.prev,
        statusCode: response.statusCode,
      })
    })
    .catch((error) => {
      console.error('Could not fetch list of rooms.', error);
      reject(error);
    })
})
