<?php

namespace Drupal\multi_session\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Multi session entities.
 *
 * @ingroup multi_session
 */
class MultiSessionDeleteForm extends ContentEntityDeleteForm {


}
