<?php
/**
 * @file
 * Installation hooks for Speakers module.
 */

use \Drupal\taxonomy\Entity\Vocabulary;

/**
 * Implements hook_install().
 */
function speakers_main_install() {
  speakers_main_keywords_vocabulary();
}

function speakers_main_keywords_vocabulary() {
  $vocabularies = Vocabulary::loadMultiple();
  if (!isset($vocabularies['keywords'])) {
    $vocabulary = Vocabulary::create(array(
      'vid' => 'keywords',
      'name' => t('Keywords'),
    ));
    $vocabulary->save();
  }
}

/**
 * Add keywords vocabulary.
 */
function speakers_main_update_8001() {
  speakers_main_keywords_vocabulary();
}

/**
 * Update Timezone users to Zurich.
 */
function speakers_main_update_8002() {
  db_update('users_field_data')
    ->fields(array(
      'timezone' => 'Europe/Zurich',
    ))
    ->execute();
}

/**
 * Migrate Paragraphs parent from User History to User.
 */
function speakers_main_update_8003() {
  $paragraphsToUpdate = \Drupal::entityTypeManager()
    ->getStorage('paragraph')
    ->loadByProperties([
      'parent_type' => 'user_history'
    ]);

  foreach ($paragraphsToUpdate as $paragraph) {
    $paragraph->save();
  }
}
